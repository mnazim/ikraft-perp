<?php
set_time_limit(45);
ini_set('max_execution_time', '45');
ini_set('max_upload_size', '64M');
ini_set('max_post_size', '64M');

$dbName = 'perp';
$dbUser = 'root';
$dbPassword = 'root';
$dbHost = '127.0.0.1';
$dbPort = '3306';

$db = new mysqli($dbHost, $dbUser, $dbPassword, $dbName, $dbPort);

if ($db->connect_error){
    die("Connection failed" . $db->connect_error);
}
