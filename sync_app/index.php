<html>
<title>PERP Sync server</title>
<body style="font-family: sans-serif; padding: 8px; margin: 8px auto">
<h1>PERP - Sync server</h1>

    <div>
    <h3>How to create db schema?</h3>
    <p>
        Create tables it using <a href="adminer.php?db=perp&username=root">DB Admin</a> with following SQL
    </p>
        <textarea readonly rows="20" cols="100"><?php include_once('schema.sql'); ?></textarea>

    <h3>How to import synced records?</h3>
    <p>
        Go to <a href="import.php">import.php</a> to import records.
        It imports <b>100</b> records at a time. To increase/decrease the number of records imported in one run,
        open <b><code>import.php</code></b> in a text editor and change <b><code>$MAX_IMPORT_COUNT</code></b> variable
        at the top of the file.
    </p>

    <h3>How to set import script to run automatically on Linux?</h3>
    <p>
        Run following command on terminal will schedule the import to run every minute.
        <textarea readonly rows="1" cols="100">(crontab -l 2>/dev/null; echo "* * * * * curl -s http://localhost/sync_app/import.php > /var/log/import-log-`date +%F-%T`.html ") | crontab -</textarea>
    </p>

    <h3>How to set import script to run automatically on Windows?</h3>
    <p>
        You'll need add a Scheduled Task from Windows Control Panel. I do not have windows, therefore I have no way of doing this.

        Search google and find a way to add a Scheduled Task that accesses import url every minute. <pre>http://localhost/sync_app/import.php</pre>
        You should also find a way to save the output html of every run to an html file so you can inspect logs.
    </p>

    <h3>How often can/should import script be called?</h3>
    <p> Schedule it to run once every minute. Smaller interval may cause race condition.</p>

    </div>
</body>
</html>