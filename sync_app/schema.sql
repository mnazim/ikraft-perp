DROP TABLE IF EXISTS `inbox`;
CREATE TABLE `inbox` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `filename` text NOT NULL,
  `status` varchar(16) NOT NULL,
  `created` bigint(20) unsigned NOT NULL,
  `modified` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `photos`;
CREATE TABLE `photos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `structure_id` bigint(20) unsigned NOT NULL,
  `filename` text NOT NULL,
  `imported_at` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `structure_id` (`structure_id`),
  CONSTRAINT `photos_ibfk_1` FOREIGN KEY (`structure_id`) REFERENCES `structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `properties`;
CREATE TABLE `properties` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `owner` text NOT NULL,
  `parentage` text DEFAULT NULL,
  `mobile_number` text DEFAULT NULL,
  `location` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `location_manual` text DEFAULT NULL,
  `address` text DEFAULT NULL,
  `district` text DEFAULT NULL,
  `village_code` text NOT NULL,
  `land_type` text NOT NULL,
  `land_classification` text NOT NULL,
  `distance_to_nala` smallint(6) DEFAULT NULL,
  `khasra_number` text NOT NULL,
  `kanals` int(10) unsigned NOT NULL,
  `marlas` decimal(10,2) unsigned NOT NULL,
  `details` text DEFAULT NULL,
  `operator` text NOT NULL,
  `rev_official1` text NOT NULL,
  `rev_official2` text NOT NULL,
  `forrest_official` text NOT NULL,
  `imported_at` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `structures`;
CREATE TABLE `structures` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `property_id` bigint(20) unsigned NOT NULL,
  `build_type` text DEFAULT NULL,
  `classification` text NOT NULL,
  `permission_type` text NOT NULL,
  `room_count` int(10) unsigned DEFAULT NULL,
  `construction_year` smallint(5) unsigned DEFAULT NULL,
  `area` int(10) unsigned DEFAULT NULL,
  `distance_to_nala` smallint(6) unsigned DEFAULT NULL,
  `details` text NOT NULL,
  `imported_at` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `structures_ibfk_2` FOREIGN KEY (`property_id`) REFERENCES `properties` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
