<?php
require_once('_mysqlConnect.php');

$method = strtolower($_SERVER['REQUEST_METHOD']);
if ($method !== 'post') {
    print('<h1>405 Method not allowed</h1>');
    header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405); //Halt the script's execution. exit;
    exit();
}

$inboxDir = './inbox';
$fileError = $_FILES['zipFile']['error'];
if ($fileError != 0) {
    error_log('BAD upload file'.print_r($_FILES, true));
    header($_SERVER["SERVER_PROTOCOL"]." 400 Bad upload file", true, 408); //Halt the script's execution. exit;
    exit();
}


$fileName = $_FILES['zipFile']['name'];
$tmpName= $_FILES['zipFile']['tmp_name'];
$fileSize = $_FILES['zipFile']['size'];

$fileDest = $inboxDir .'/'.$fileName;

$moved = move_uploaded_file($tmpName, $fileDest);


if (!$moved){
    error_log('BAD upload file'.print_r($_FILES, true));
    header($_SERVER["SERVER_PROTOCOL"]." 400 Bad upload file", true, 408); //Halt the script's execution. exit;
    exit();
}
$now = time();
$query = "INSERT INTO inbox (filename, status, created, modified) VALUES ('".$fileName."', 'pending', $now, $now)";
error_log($query);

$res = $db->query($query);

error_log(print_r($res, true));
error_log(print_r(mysqli_error($db), true));

if (!$res) {
    error_log('Internal server error'.print_r($res, true));
    header($_SERVER["SERVER_PROTOCOL"]." 500 Internal Server Error", true, 500); //Halt the script's execution. exit;
    exit();
}
