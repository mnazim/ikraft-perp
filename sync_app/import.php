<html>
<head><title>PERP Import at <?=date("Y-m-d-H-i-s")?></title></head>
<body style="font-family: sans-serif; margin: 0 auto;">
<?php
// Tell mysqli to throw an exception if an error occurs
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

require_once('_mysqlConnect.php');

$PHOTOS_DIR = './photos';
$TMP_DIR = './tmp';
$MAX_IMPORT_COUNT = 100;

function start() {
    select_for_import();
    import_selected();
}

function select_for_import() {
    global $db, $MAX_IMPORT_COUNT;
    $query = "UPDATE inbox SET status = 'started' WHERE status = 'pending' LIMIT ".$MAX_IMPORT_COUNT;
    $res = $db->query($query);
}

function import_selected() {
    global $db, $MAX_IMPORT_COUNT;
    $query = "SELECT * FROM inbox where status = 'started' LIMIT ".$MAX_IMPORT_COUNT;
    $res = $db->query($query);
    $records = $res->fetch_all(MYSQLI_ASSOC);
    $count = count($records);
    if ($count ==0) {
        print('<h1>No records to import.</h1>');
    } else {
        print('<h1>Importing '.$count.' records.</h1>');
    }
    foreach($records as $record) {
        import_record($record);
    }
}

function import_record($record) {
    $css = 'background: #eee; color: #333; font-family: sans-serif; font-size: 13px; padding: 8px; margin: 4px';
    print('<div style="'.$css.'">');
    global $TMP_DIR, $db;
    $id = $record['id'];
    $zipPath = "inbox/".$record['filename'];
    $info = pathinfo($zipPath);
    $dirname = $info['filename'];
    $dir = $TMP_DIR.'/'.$dirname;
    $jsonFile = $dir."/data.json";

    print(t('p', 'Importing record <b>id: '.$id.'</b>, <b>file path: '.$zipPath.'</b>'));

    // Extract zip file
    if(!file_exists($zipPath))  {
        print(t('p', 'File not found! Skipping.'));
        mark_status($id,'missing_zipfile');
        print(t('div', result_label(false)));
        print('</div>');
        return;
    }

    $extractor = new ZipArchive;

    if ($extractor->open($zipPath) === TRUE) {
        $extractor->extractTo($dir);
        $extractor->close();
    } else {
        print(t('p','Corrupted zip file! Skipping'));
        mark_status($id, 'corrupted_zipfile');
        print(t('div', result_label(false)));
        print('</div>');
        return;
    }
    print(t('p', 'Extracted <b>'.$zipPath.'</b>'));

    // Import json
    if (!file_exists($jsonFile)) {
        print(t('p', $jsonFile.' file not found in zip file! Skipping.'));
        mark_status($id,'missing_json');
        print(t('div', result_label(false)));
        print('</div>');
        return;
    }

    $json = file_get_contents($jsonFile);
    $property = json_decode($json);

    $db->autocommit(false);
    $res = import_property($property, $dir);
    $db->autocommit(true);

    if ($res){
        mark_status($id, 'success');
        print(t('span', result_label(TRUE)));
    } else {
        mark_status($id, 'failed');
        print(t('span', result_label(FALSE)));
    }
    print('</div>');
}

function import_property($property, $dir) {
    global $db;
    $db->begin_transaction();
    try {
        $prop_query = build_property_insert_query($property);
        // print(t('p', 'Executing property insert query: '));
        // print(t('pre', $prop_query));
        $db->query($prop_query);
        $property_id = $db->insert_id;
        foreach($property->structures as $structure){
            $struct_query = build_structure_insert_query($structure, $property_id);
            // print(t('p', 'Executing structure insert query: '));
            // print(t('pre', $struct_query));
            $db->query($struct_query);
            $struct_id = $db->insert_id;

            foreach($structure->photos as $index => $photo) {
                $src = $dir.'/'.$photo;
                $ext = pathinfo($src, PATHINFO_EXTENSION);
                $filename = $property->village_code.'-'.$property_id.'-'.$struct_id.'-'.$index.'.'.$ext;
                $dest = 'photos/'.$filename;
                rename($src, $dest);
                $photo_query = build_photo_insert_query($filename, $struct_id);
                // print(t('pre', $photo_query));
                $db->query($photo_query);
            };
        }
        $db->commit();
        return TRUE;
    } catch (mysqli_sql_exception $exception) {
        $db->rollback();
        print(t('pre', $exception));
        return FALSE;
    }
}

function build_property_insert_query($property) {
    $now = time();
    $nala = $property->distance_to_nala !== NULL ? $property->distance_to_nala : 'NULL';
    return "INSERT INTO properties (
                owner,
                parentage,
                mobile_number,
                location,
                location_manual,
                address,
                district,
                village_code,
                land_type,
                land_classification,
                distance_to_nala,
                khasra_number,
                kanals,
                marlas,
                details,
                operator,
                rev_official1,
                rev_official2,
                forrest_official,
                imported_at
            )
            VALUES (
                '".$property->owner."',
                '".$property->parentage."',
                '".$property->mobile_number."',
                '".$property->location."',
                '".$property->location_manual."',
                '".$property->address."',
                '".$property->district."',
                '".$property->village_code."',
                '".$property->land_type."',
                '".$property->land_classification."',
                ".$nala.",
                '".$property->khasra_number."',
                ".$property->kanals.",
                ".$property->marlas.",
                '".$property->details."',
                '".$property->operator."',
                '".$property->rev_official1."',
                '".$property->rev_official2."',
                '".$property->forrest_official."',
                ".$now."
            )";
}

function build_structure_insert_query($structure, $property_id) {
    $now = time();
    $room_count= $structure->room_count !== NULL ? $structure->room_count : 'NULL';
    $construction_year = $structure->construction_year !== NULL ? $structure->construction_year : 'NULL';
    $area = $structure->area !== NULL ? $structure->area : 'NULL';
    $nala = $structure->distance_to_nala !== NULL ? $structure->distance_to_nala : 'NULL';
    return "INSERT INTO structures (
            property_id,
            build_type,
            classification,
            permission_type,
            room_count,
            construction_year,
            area,
            distance_to_nala,
            details,
            imported_at
        )
        VALUES (
            ".$property_id.",
            '".$structure->build_type."',
            '".$structure->classification."',
            '".$structure->permission_type."',
            ".$room_count.",
            ".$construction_year.",
            ".$area.",
            ".$nala.",
            '".$structure->details."',
            ".$now."
        )";

}

function build_photo_insert_query($photo, $structure_id) {
    $now = time();
    return "INSERT INTO photos (
        structure_id,
        filename,
        imported_at
    )
    VALUES (
        ".$structure_id.",
        '".$photo."',
        ".$now."
    )";
}

function mark_status($id, $status) {
    global $db;
    $query = "UPDATE inbox SET status = '".$status."' WHERE id = '".$id."'";
    $db->query($query);
}

function t($tag, $value) {
    return '<'.$tag.'>'.$value.'</'.$tag.'>';
}

function result_label($is_success) {
    $bg = $is_success ? 'green': 'red';
    $label= $is_success ? 'success': 'failed';
    return '<h4 style="background: '.$bg.';
                       color: white;
                       width: 80px;
                       text-align: center;
                       text-transform:uppercase;
            ">'.$label.'</h4>';
}

start();
?>
</body>
</html>