import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:location/location.dart';
import 'package:perp/constants.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class LocationState {
  void init(LocationData data) {
    if (data != null) {
      location = data;
    }
  }

  LocationData location;
  final Location _locator = Location();

  Future<LocationData> locate() async {
    location = await _locator.getLocation();
    return location;
  }

  Map<String, double> toJson() {
    Map<String, double> json = {
      'latitude': 0.0,
      'longitude': 0.0,
      'accuracy': 0.0,
      'altitude': 0.0,
      'speed': 0.0,
      'speedAccuracy': 0.0,
      'heading': 0.0,
    };
    if (location != null) {
      json['latitude'] = location.latitude;
      json['longitude'] = location.longitude;
      json['accuracy'] = location.accuracy;
      json['altitude'] = location.altitude;
      json['speed'] = location.speed;
      json['speedAccuracy'] = location.speedAccuracy;
      json['heading'] = location.heading;
    }
    return json;
  }
}

final Injected<LocationState> locationRM = RM.inject(() => LocationState());

class LocationField extends StatelessWidget {
  LocationField({Key key, this.location, this.onChanged}) : super(key: key);
  final Location locator = Location();
  final isLocating = RM.create(false);
  final LocationData location;
  final Function(LocationData) onChanged;

  @override
  Widget build(BuildContext context) {
    return locationRM.rebuilder(() {
      return FormBuilderCustomField(
        attribute: 'location',
        formField: FormField(
          enabled: true,
          initialValue: locationRM.state.toJson(),
          builder: (fieldState) {
            return InkWell(
              onTap: () => locationRM.setState((s) => s.locate(),
                  onData: (BuildContext context, LocationState state) {
                fieldState.didChange(state.toJson());
                onChanged(state.location);
              }),
              child: InputDecorator(
                decoration: kInputDecoration.copyWith(
                    labelText: 'Location',
                    hintText: 'Tap to locate',
                    suffixIcon: _buildAccuracy(),
                    errorText: fieldState.errorText,
                    prefixIcon: _buildIcon()),
                child: locationRM.whenRebuilder(
                  onData: () => _buildValue(),
                  onError: (e) => Text('$e'),
                  onIdle: () => _buildValue(),
                  onWaiting: () => Text('Locating...'),
                ),
              ),
            );
          },
        ),
      );
    }, initState: () {
      locationRM.setState((s) => s.init(location), onData: (c, s) {
        onChanged(s.location);
      });
    });
  }

  Widget _buildIcon() {
    var icon = Icon(Icons.my_location);
    return locationRM.whenRebuilderOr(
      onWaiting: () {
        return SizedBox(
          height: 32,
          width: 32,
          child: Stack(
            alignment: Alignment.center,
            children: [
              icon,
              CircularProgressIndicator(),
            ],
          ),
        );
      },
      builder: () {
        return Icon(Icons.my_location);
      },
    );
  }

  Widget _buildValue() {
    final loc = locationRM.state.location;
    var text = loc == null ? 'Tap to locate' : '${loc.latitude},${loc.longitude}';
    return Text(text);
  }

  Widget _buildAccuracy() {
    var loc = locationRM.state.location;
    if (loc == null) return null;
    int accuracy = loc.accuracy.floor();
    var color = Colors.green;
    if (accuracy == null) {
      color = Colors.green;
    } else if (accuracy > 15) {
      color = Colors.red;
    }
    return Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: Container(
        color: color,
        padding: EdgeInsets.symmetric(horizontal: 4),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              'accuracy'.toUpperCase(),
              style: TextStyle(color: Colors.white, fontSize: 12),
            ),
            Text(
              '$accuracy meter',
              style: TextStyle(color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }
}
