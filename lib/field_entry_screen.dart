import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get_it/get_it.dart';
import 'package:location/location.dart';
import 'package:moor/moor.dart' as moor;
import 'package:path/path.dart' as path;
import 'package:perp/_property_form.dart';
import 'package:perp/_shimmer.dart';
import 'package:perp/_structure_form.dart';
import 'package:perp/db.dart';
import 'package:perp/external_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class FieldEntryScreenState {
  FieldEntryScreenState();
  Property property;
  List<Structure> structures = [];
  Structure currentlyEditingStructure;
  bool isReady = false;
  Directory photosDir;
  final GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();
  String operator;
  String revOfficial1;
  String revOfficial2;
  String forrestOfficial;
  String selectedVillage;
  String district = 'Anantnag';

  PerpDatabase get db => GetIt.I.get<PerpDatabase>();
  bool get isNotReady => !isReady;

  void init(int propertyId) async {
    if (propertyId != null) {
      property = await db.getProperty(propertyId);
      // print('---> loaded property $property');
      db.getPropertyStructures(propertyId).then((rows) {
        structures = rows;
      });
      photosDir = await ExternalStorage.getPhotosDir();
      if (!villagePhotosDir.existsSync()) {
        villagePhotosDir.createSync(recursive: true);
      }
    }
    _setupOperator();
    isReady = true;
  }

  void _setupOperator() {
    var prefs = GetIt.I.get<SharedPreferences>();
    operator = prefs.getString('operatorName');
    revOfficial1 = prefs.getString('revOfficial1');
    revOfficial2 = prefs.getString('revOfficial2');
    forrestOfficial = prefs.getString('forrestOfficial');
    selectedVillage = prefs.getString('villageCode');
  }

  Directory get villagePhotosDir => Directory(path.join(photosDir.path, property.villageCode));

  void saveProperty(Map<String, dynamic> json) async {
    PropertiesCompanion companion =
        property != null ? property.toCompanion(true) : PropertiesCompanion();
    companion = companion.copyWith(
      location: moor.Value<String>(jsonEncode(json['location'])),
      locationManual: moor.Value<String>(json['locationManual']),
      owner: moor.Value<String>(json['owner']),
      parentage: moor.Value<String>(json['parentage']),
      mobileNumber: moor.Value<String>(json['mobileNumber']),
      address: moor.Value<String>(json['address']),
      villageCode: moor.Value<String>(json['villageCode']),
      landType: moor.Value<String>(json['landType']),
      landClassification: moor.Value<String>(json['landClassification']),
      khasraNumber: moor.Value<String>(json['khasraNumber']),
      distanceToNala: moor.Value<int>(int.tryParse(json['distanceToNala'])),
      kanals: moor.Value<int>(int.tryParse(json['kanals'])),
      marlas: moor.Value<double>(double.tryParse(json['marlas'])),
      details: moor.Value<String>(json['details']),
      operator: moor.Value<String>(operator),
      revOfficial1: moor.Value<String>(revOfficial1),
      revOfficial2: moor.Value<String>(revOfficial2),
      forrestOfficial: moor.Value<String>(forrestOfficial),
    );
    if (property == null) {
      property = await db.addProperty(companion);
    } else {
      property = await db.updateProperty(companion);
    }
  }

  void saveStructure(Map<String, dynamic> json) async {
    StructuresCompanion companion = currentlyEditingStructure != null
        ? currentlyEditingStructure.toCompanion(true)
        : StructuresCompanion();

    var photos = _saveStructurePhotos(List<File>.from(json['photos']));
    companion = companion.copyWith(
      propertyId: moor.Value<int>(property.id),
      photos: moor.Value<String>(jsonEncode(photos)),
      // location: moor.Value<String>(jsonEncode(json['location'])),
      // locationManual: moor.Value<String>(json['locationManual']),
      location: moor.Value<String>('{}'),
      locationManual: moor.Value<String>(''),
      distanceToNala: moor.Value<int>(int.tryParse(json['distanceToNala'])),
      constructionYear: moor.Value<int>(int.tryParse(json['constructionYear'])),
      area: moor.Value<int>(int.tryParse(json['area'])),
      roomCount: moor.Value<int>(int.tryParse(json['roomCount'])),
      buildType: moor.Value<String>(json['buildType']),
      classification: moor.Value<String>(json['classification']),
      permissionType: moor.Value<String>(json['permissionType']),
      details: moor.Value<String>(json['details']),
      operator: moor.Value<String>(operator),
      revOfficial1: moor.Value<String>(revOfficial1),
      revOfficial2: moor.Value<String>(revOfficial2),
      forrestOfficial: moor.Value<String>(forrestOfficial),
    );

    if (currentlyEditingStructure == null) {
      await db.addStructure(companion);
    } else {
      await db.updateStructure(companion);
    }

    structures = await db.getPropertyStructures(property.id);
    currentlyEditingStructure = null;
  }

  void deleteStructure(int id) async {
    await db.deleteStructure(id);
    structures = await db.getPropertyStructures(property.id);
  }

  List<String> _saveStructurePhotos(List<File> photos) {
    var saved = <String>[];
    for (File photo in photos) {
      var filename = path.basename(photo.path);
      var dest = path.join(villagePhotosDir.path, filename);
      if (photo.path != dest) photo.copySync(dest);
      saved.add(path.join(property.villageCode, filename));
    }
    return saved;
  }

  List<File> loadPhotos(Structure structure) {
    var photoStr = structure?.photos ?? null;
    List<String> photos = photoStr != null ? List<String>.from(jsonDecode(photoStr)) : <String>[];
    return photos.map((p) => File(path.join(photosDir.path, p))).toList();
  }

  LocationData jsonToLocation(String locationString) {
    if (locationString?.isEmpty ?? true) return null;
    try {
      var json = jsonDecode(locationString);
      return LocationData.fromMap(Map<String, double>.from(json));
    } catch (e) {
      return null;
    }
  }
}

final entryScreenState = RM.inject(() => FieldEntryScreenState());

class FieldEntryScreen extends StatelessWidget {
  FieldEntryScreen({Key key, this.propertyId});

  final int propertyId;

  @override
  Widget build(BuildContext context) {
    return entryScreenState.rebuilder(
      () {
        Widget body;
        Widget bottomBar;
        if (entryScreenState.state.isNotReady) {
          body = ShimmerBuilder();
          bottomBar = Container();
        } else if (entryScreenState.state.property == null) {
          body = PropertyForm();
          bottomBar = Container();
        } else {
          body = _buildBody(context);
          bottomBar = BottomAppBar(
            color: Colors.transparent,
            elevation: 0,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: RaisedButton.icon(
                icon: Icon(Icons.local_attraction_outlined),
                label: Text('Add a structure'),
                onPressed: () {
                  RM.navigate.to(StructureForm());
                },
              ),
            ),
          );
        }
        return Scaffold(
          body: body,
          bottomNavigationBar: bottomBar,
        );
      },
      initState: () {
        entryScreenState.setState((s) => s.init(propertyId));
      },
    );
  }

  Widget _buildBody(BuildContext context) {
    var slivers = <Widget>[
      _buildPropertyTile(context),
      _buildStructureTiles(context),
    ].map((s) => SliverToBoxAdapter(child: s)).toList();
    return CustomScrollView(
      slivers: [
        SliverAppBar(
          title: Text('Field Entry'),
          floating: true,
        ),
        ...slivers,
      ],
    );
  }

  Widget _buildPropertyTile(BuildContext context) {
    return InkWell(
      child: PropertyTile(property: entryScreenState.state.property),
      onTap: () {
        RM.navigate.to(PropertyForm());
      },
    );
  }

  Widget _buildStructureTiles(BuildContext context) {
    var structureTiles = entryScreenState.state.structures.map((st) => Container(
          child: _buildStructureTile(context, st),
        ));

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
              '${structureTiles.length} Associated Structure${structureTiles.length == 1 ? '' : 's'}',
              style: Theme.of(context).textTheme.headline6),
        ),
        if (structureTiles.isEmpty)
          Text(
            'No structures are associated with this property',
            style: TextStyle(fontStyle: FontStyle.italic),
          )
        else
          ...structureTiles,
      ],
    );
  }

  Widget _buildStructureTile(BuildContext context, Structure structure) {
    return InkWell(
      child: StructureTile(structure: structure),
      onTap: () {
        entryScreenState.setState(
          (s) => s.currentlyEditingStructure = structure,
          onData: (_, __) => RM.navigate.to(StructureForm()),
        );
      },
    );
  }
}

class PropertyTile extends StatelessWidget {
  PropertyTile({Key key, this.property}) : super(key: key);

  final Property property;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          if (property.syncedOn > 0)
            Container(
              color: Colors.green,
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              child: Center(
                child: Text(
                  'Synced on: ${DateTime.fromMillisecondsSinceEpoch(property.syncedOn * 1000)}',
                  style: TextStyle(
                    fontFamily: 'monospace',
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text('Name', style: Theme.of(context).textTheme.caption),
            Text(property.owner, style: Theme.of(context).textTheme.headline5),
          ]),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text('Parentage', style: Theme.of(context).textTheme.caption),
            Text(property.parentage ?? '', style: Theme.of(context).textTheme.bodyText1),
          ]),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text('Mobile Number', style: Theme.of(context).textTheme.caption),
            Text(property.mobileNumber ?? '', style: Theme.of(context).textTheme.bodyText1),
          ]),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text('Land Area/Type/Classification', style: Theme.of(context).textTheme.caption),
            Text(
                '${property.kanals} kanal & ${property.marlas} marlaa ${property.landType}/${property.landClassification}',
                style: Theme.of(context).textTheme.bodyText1),
          ]),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text('Address/Village', style: Theme.of(context).textTheme.caption),
            Text('${property.address}, ${property.villageCode}',
                style: Theme.of(context).textTheme.bodyText1),
          ]),
        ]
            .map((c) => Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4),
                  child: c,
                ))
            .toList(),
      ),
    );
  }
}

class StructureTile extends StatelessWidget {
  StructureTile({Key key, this.structure});
  final Structure structure;

  @override
  Widget build(BuildContext context) {
    var photos = entryScreenState.state.loadPhotos(structure);
    var leadPhoto = photos?.isNotEmpty ?? false ? photos.first : null;
    return Card(
        child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListTile(
        leading: SizedBox(
          height: 64,
          width: 64,
          child: leadPhoto != null
              ? Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(image: FileImage(leadPhoto), fit: BoxFit.cover)),
                )
              : Center(
                  child: Icon(Icons.photo),
                ),
        ),
        title: Text('${structure.details}'),
        subtitle: Text(
            'Classification: ${structure.classification}, Permission: ${structure.permissionType}'),
        trailing: IconButton(
            icon: Icon(Icons.delete, color: Colors.red),
            onPressed: () {
              entryScreenState.setState((s) => s.deleteStructure(structure.id));
            }),
      ),
    ));
  }
}
