import 'dart:async';
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:perp/constants.dart';
import 'package:path/path.dart' as path;

class ExternalStorage {
  static const MethodChannel _channel = MethodChannel('com.ikraftsoft.perp');

  static Future<String> getExternalFilesDir() async {
    if (!Platform.isAndroid) {
      throw UnsupportedError("Only android supported");
    }
    return await _channel.invokeMethod('getExternalFilesDir');
  }

  static Future<Directory> getAppStorageDir() async {
    var extDir = await getExternalFilesDir();
    var appDir = Directory(path.join(extDir, kAppDir));
    if (!appDir.existsSync()) {
      appDir.createSync(recursive: true);
    }
    return appDir;
  }

  static Future<Directory> getPhotosDir() async {
    var appDir = await getAppStorageDir();
    var photosDir = Directory(path.join(appDir.path, 'photos'));
    if (!photosDir.existsSync()) {
      photosDir.createSync(recursive: true);
    }
    return photosDir;
  }

  static Future<Directory> getSyncDir() async {
    var appDir = await getAppStorageDir();
    var syncDir = Directory(path.join(appDir.path, 'sync'));
    if (!syncDir.existsSync()) {
      syncDir.createSync(recursive: true);
    }
    return syncDir;
  }

  static Future<File> getDBFile() async {
    var appDir = await getAppStorageDir();
    return File(path.join(appDir.path, 'PERP.sqlite'));
  }
}
