import 'dart:async';

import 'package:flutter/material.dart';
import 'package:perp/db.dart';
import 'package:perp/home.dart';
import 'package:perp/injectables.dart';
import 'package:perp/permissions.dart';
import 'package:perp/settings.dart';
import 'package:sentry/sentry.dart';
import 'package:get_it/get_it.dart' show GetIt;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

final _sentry =
    SentryClient(dsn: "https://74ff2593ba374e93b133376669e46a0e@o445303.ingest.sentry.io/5421527");

bool get inDebugMode {
  var debugMode = false;
  // INFO: asserts are stripped in release mode.
  assert(debugMode = true);
  return debugMode;
}

Future<Null> _reportError(dynamic error, dynamic stackTrace) async {
  if (inDebugMode) {
    debugPrint('${'⚠ ' * 40}');
    debugPrint(error.toString());
    debugPrint(stackTrace.toString());
    return;
  }
  final response = await _sentry.captureException(
    exception: error,
    stackTrace: stackTrace,
  );
  if (response.isSuccessful) {
    debugPrint('Exception reported to sentry. Event ID: ${response.eventId}');
  } else {
    debugPrint('Reporting exception to sentry failed: ${response.error}');
  }
}

void main() async {
  FlutterError.onError = (FlutterErrorDetails details) async {
    if (inDebugMode) {
      FlutterError.dumpErrorToConsole(details);
    } else {
      Zone.current.handleUncaughtError(details.exception, details.stack);
    }
  };

  return runZonedGuarded<Future<Null>>(() async {
    WidgetsFlutterBinding.ensureInitialized();
    GetIt.I.registerSingleton<PerpDatabase>(PerpDatabase());
    GetIt.I.registerSingletonAsync<SharedPreferences>(
        () async => await SharedPreferences.getInstance());
    runApp(App());
  }, (error, stackTrace) async {
    await _reportError(error, stackTrace);
  });
}

final getItReady = RM.injectFuture<bool>(() async {
  await GetIt.I.allReady();
  return true;
});

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return getItReady.whenRebuilder(
        onWaiting: () => Center(child: CircularProgressIndicator()),
        onError: (e) => Text('$e'),
        onIdle: () => Text('idle'),
        onData: () => PermService.whenRebuilder(
              onData: () => _getApp(context),
              onWaiting: () => Center(child: CircularProgressIndicator()),
              onError: (e) => Text('$e'),
              onIdle: () => Text('idle'),
            ));
  }

  Widget _getApp(BuildContext context) => MaterialApp(
        title: 'PERP Field Entry',
        theme: ThemeData(
          primarySwatch: Colors.green,
          canvasColor: Colors.orange.shade100,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          buttonTheme: Theme.of(context).buttonTheme.copyWith(
              buttonColor: Colors.green,
              textTheme: ButtonTextTheme.accent,
              colorScheme: Theme.of(context).colorScheme.copyWith(secondary: Colors.white)),
        ),
        navigatorKey: RM.navigate.navigatorKey,
        initialRoute: HomeScreen.routeName,
        routes: {
          HomeScreen.routeName: (_) => HomeScreen(),
          PermissionsScreen.routeName: (_) => PermissionsScreen(),
          SettingsScreen.routeName: (_) => SettingsScreen(),
        },
      );
}
