import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:perp/constants.dart';
import 'package:perp/db.dart';
import 'package:perp/field_entry_screen.dart';
import 'package:perp/injectables.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

final GlobalKey<FormBuilderState> _formKey = GlobalKey<FormBuilderState>();

class StructureForm extends StatelessWidget {
  // final _focusLocation = FocusNode();
  final _focusYear = FocusNode();
  final _focusArea = FocusNode();
  final _focusRooms = FocusNode();
  final _focusNala = FocusNode();
  final _focusDetails = FocusNode();

  Structure get structure => entryScreenState.state.currentlyEditingStructure;

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: _buildBody(context));
  }

  Widget _buildBody(BuildContext context) {
    final fields = <Widget>[
      _buildImagePickerField(),
      // _buildStructureLocationField(context),
      // _buildStructureManualLocationField(context),
      _buildStructureConstructionYearField(context),
      _buildStructureAreaField(context),
      _buildStructureRoomCountField(context),
      _buildStructureDistanceToNalaField(context),
      _buildStructureDetailsField(context),
      _buildStructureBuildTypesField(context),
      _buildStructureClassificationsField(context),
      _buildStructurePermissionTypesField(context),
      _buildSaveStructureButton(context),
    ]
        .map((f) =>
            SliverToBoxAdapter(child: Padding(padding: const EdgeInsets.all(8.0), child: f)))
        .toList();
    return FormBuilder(
      key: _formKey,
      child: CustomScrollView(
        slivers: [
          SliverAppBar(
            title: Text('Structure form'),
            floating: true,
          ),
          ...fields
        ],
      ),
    );
  }

  Widget _buildImagePickerField() {
    return FormBuilderImagePicker(
      attribute: 'photos',
      initialValue: entryScreenState.state.loadPhotos(structure),
      decoration: kInputDecoration.copyWith(labelText: 'Photos'),
      validators: [FormBuilderValidators.required()],
    );
  }

  // Widget _buildStructureLocationField(BuildContext context) {
  //   var locationString = structure?.location ?? '';

  //   return LocationField(
  //     location: entryScreenState.state.jsonToLocation(locationString),
  //     onChanged: (value) {
  //       FocusScope.of(context).requestFocus(_focusLocation);
  //     },
  //   );
  // }

  // Widget _buildStructureManualLocationField(BuildContext context) {
  //   return FormBuilderTextField(
  //     attribute: 'locationManual',
  //     initialValue: structure?.locationManual ?? '',
  //     focusNode: _focusLocation,
  //     decoration: kInputDecoration.copyWith(
  //       labelText: 'Latitude,Longitude pair',
  //       prefixIcon: Icon(Icons.pin_drop),
  //     ),
  //     autovalidateMode: AutovalidateMode.always,
  //     textInputAction: TextInputAction.next,
  //     onFieldSubmitted: (v) => FocusScope.of(context).requestFocus(_focusYear),
  //     // valueTransformer: (v) => moor.Value<String>(v),
  //   );
  // }

  Widget _buildStructureBuildTypesField(BuildContext context) {
    return allStructureBuildTypes.whenRebuilder(
      onIdle: () => Text('idle'),
      onError: (e) => Text('$e'),
      onWaiting: () => Container(),
      onData: () {
        var items = allStructureBuildTypes.state
            .map((bt) => DropdownMenuItem(
                child: Text(bt, maxLines: 2, overflow: TextOverflow.clip), value: bt))
            .toList();
        return FormBuilderDropdown(
          attribute: 'buildType',
          decoration: kInputDecoration.copyWith(
            labelText: 'Select a structure build type',
            prefixIcon: Icon(Icons.filter_alt),
          ),
          autovalidateMode: AutovalidateMode.always,
          initialValue: structure?.buildType ?? null,
          items: items,
        );
      },
    );
  }

  Widget _buildStructureClassificationsField(BuildContext context) {
    return allStructureClassifications.whenRebuilder(
      onIdle: () => Text('idle'),
      onError: (e) => Text('$e'),
      onWaiting: () => Container(),
      onData: () {
        var items = allStructureClassifications.state
            .map((cls) => DropdownMenuItem(
                child: Text(cls, maxLines: 2, overflow: TextOverflow.clip), value: cls))
            .toList();
        return FormBuilderDropdown(
          attribute: 'classification',
          decoration: kInputDecoration.copyWith(
            labelText: 'Select a structure classification',
            prefixIcon: Icon(Icons.filter_alt),
          ),
          validators: [FormBuilderValidators.required()],
          autovalidateMode: AutovalidateMode.always,
          initialValue: structure?.classification ?? null,
          items: items,
        );
      },
    );
  }

  Widget _buildStructurePermissionTypesField(BuildContext context) {
    return allPermissionTypes.whenRebuilder(
      onIdle: () => Text('idle'),
      onError: (e) => Text('$e'),
      onWaiting: () => Container(),
      onData: () {
        var items = allPermissionTypes.state
            .map((perm) => DropdownMenuItem(
                child: Text(perm, maxLines: 2, overflow: TextOverflow.clip), value: perm))
            .toList();
        return FormBuilderDropdown(
          attribute: 'permissionType',
          decoration: kInputDecoration.copyWith(
            labelText: 'Select a permission type',
            prefixIcon: Icon(Icons.filter_alt),
          ),
          validators: [FormBuilderValidators.required()],
          autovalidateMode: AutovalidateMode.always,
          initialValue: structure?.permissionType ?? null,
          items: items,
        );
      },
    );
  }

  Widget _buildStructureDistanceToNalaField(BuildContext context) {
    return FormBuilderTextField(
      attribute: 'distanceToNala',
      initialValue: structure?.distanceToNala?.toString() ?? null,
      decoration: kInputDecoration.copyWith(
        labelText: 'Distance from nala',
        prefixIcon: Icon(Icons.home),
        suffixIcon: Text('meter'),
      ),
      focusNode: _focusNala,
      keyboardType: TextInputType.number,
      onFieldSubmitted: (v) => FocusScope.of(context).requestFocus(_focusDetails),
      textInputAction: TextInputAction.next,
      autovalidateMode: AutovalidateMode.always,
      // valueTransformer: (v) => moor.Value<int>(int.tryParse(v)),
    );
  }

  Widget _buildStructureDetailsField(BuildContext context) {
    return FormBuilderTextField(
      maxLines: 3,
      decoration: kInputDecoration.copyWith(
        labelText: 'Details',
        hintText: "Any other details",
        prefixIcon: Icon(Icons.home),
      ),
      focusNode: _focusDetails,
      attribute: 'details',
      initialValue: structure?.details ?? null,
      validators: [FormBuilderValidators.required()],
      autovalidateMode: AutovalidateMode.always,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (v) => FocusScope.of(context).unfocus(),
      // valueTransformer: (v) => moor.Value<String>(v),
    );
  }

  Widget _buildStructureRoomCountField(BuildContext context) {
    return FormBuilderTextField(
      attribute: 'roomCount',
      focusNode: _focusRooms,
      decoration: kInputDecoration.copyWith(
        labelText: 'Number of rooms',
        prefixIcon: Icon(Icons.home),
      ),
      validators: [
        FormBuilderValidators.numeric(),
        FormBuilderValidators.min(1),
      ],
      autovalidateMode: AutovalidateMode.always,
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      initialValue: structure?.roomCount?.toString() ?? null,
      onFieldSubmitted: (v) => FocusScope.of(context).requestFocus(_focusNala),
      // valueTransformer: (v) => moor.Value<int>(int.tryParse(v)),
    );
  }

  Widget _buildStructureConstructionYearField(BuildContext context) {
    return FormBuilderTextField(
      attribute: 'constructionYear',
      focusNode: _focusYear,
      decoration: kInputDecoration.copyWith(
        labelText: 'Year of construction',
        prefixIcon: Icon(Icons.home),
      ),
      validators: [
        FormBuilderValidators.numeric(),
        FormBuilderValidators.min(1901),
        FormBuilderValidators.max(DateTime.now().year),
      ],
      autovalidateMode: AutovalidateMode.always,
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      initialValue: structure?.constructionYear?.toString() ?? null,
      onFieldSubmitted: (v) => FocusScope.of(context).requestFocus(_focusArea),
      // valueTransformer: (v) => moor.Value<int>(int.tryParse(v)),
    );
  }

  Widget _buildStructureAreaField(BuildContext context) {
    return FormBuilderTextField(
      focusNode: _focusArea,
      attribute: 'area',
      autovalidateMode: AutovalidateMode.always,
      decoration: kInputDecoration.copyWith(
          labelText: "Area (sqft)", hintText: "Area of the structure in square feet"),
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (v) => FocusScope.of(context).requestFocus(_focusRooms),
      initialValue: structure?.area?.toString() ?? null,
      // valueTransformer: (v) => moor.Value<int>(int.tryParse(v)),
    );
  }

  Widget _buildSaveStructureButton(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        RaisedButton(
          color: Colors.red,
          child: Text('Discard Changes'),
          onPressed: () {
            RM.navigate.back();
          },
        ),
        RaisedButton(
          child: Text('Save Changes'),
          onPressed: () {
            if (_formKey.currentState.saveAndValidate()) {
              entryScreenState.setState(
                (s) => s.saveStructure(_formKey.currentState.value),
                onData: (context, state) {
                  RM.navigate.back();
                  RM.scaffoldShow.snackBar(SnackBar(
                    content: Text('Structure saved. You may enter more structures.'),
                  ));
                },
                onError: (context, error) {
                  print("---> $error");
                  RM.scaffoldShow.snackBar(SnackBar(
                    backgroundColor: Colors.red,
                    content: Text('Error: Not saved.'),
                  ));
                },
              );
            }
          },
        ),
      ],
    );
  }
}
