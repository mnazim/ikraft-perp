import 'package:get_it/get_it.dart';
import 'package:perp/db.dart';
import 'package:perp/permissions.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

// ignore: non_constant_identifier_names
final Injected<PermissionsService> PermService =
    RM.injectFuture(() => PermissionsService().init());

final Injected<List<Map<String, dynamic>>> allVillagesRM =
    RM.injectFuture(GetIt.I.get<PerpDatabase>().allVillages);

final Injected<List<String>> allLandTypesRM =
    RM.injectFuture(GetIt.I.get<PerpDatabase>().allLandTypes);

final Injected<List<String>> allLandClassifications =
    RM.injectFuture(GetIt.I.get<PerpDatabase>().allLandClassifications);

final Injected<List<String>> allStructureBuildTypes =
    RM.injectFuture(GetIt.I.get<PerpDatabase>().allStructureBuildTypes);

final Injected<List<String>> allStructureClassifications =
    RM.injectFuture(GetIt.I.get<PerpDatabase>().allStructureClassifications);

final Injected<List<String>> allPermissionTypes =
    RM.injectFuture(GetIt.I.get<PerpDatabase>().allPermissionTypes);
