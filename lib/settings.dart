import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsScreen extends StatelessWidget {
  static final routeName = '/settings';
  final GlobalKey<FormBuilderState> _formKey = GlobalKey<FormBuilderState>();
  final FocusNode _focusOperator = FocusNode();
  final FocusNode _focusRevOfficial1 = FocusNode();
  final FocusNode _focusRevOfficial2 = FocusNode();
  final FocusNode _focusForrestOfficer = FocusNode();
  final FocusNode _focusSync = FocusNode();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var prefs = GetIt.I.get<SharedPreferences>();
    var operator = prefs.getString('operatorName');
    var revOfficial1 = prefs.getString('revOfficial1');
    var revOfficial2 = prefs.getString('revOfficial2');
    var forrestOfficial = prefs.getString('forrestOfficial');
    var syncServer = prefs.getString('syncServer');
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Settings'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              FormBuilder(
                key: _formKey,
                child: Column(
                  children: [
                    FormBuilderTextField(
                      decoration: InputDecoration(labelText: 'Data Entry Operator'),
                      initialValue: operator,
                      focusNode: _focusOperator,
                      attribute: 'operator',
                      autovalidateMode: AutovalidateMode.always,
                      textInputAction: TextInputAction.next,
                      validators: [FormBuilderValidators.required()],
                      onFieldSubmitted: (v) {
                        FocusScope.of(context).requestFocus(_focusRevOfficial1);
                      },
                    ),
                    FormBuilderTextField(
                      decoration: InputDecoration(labelText: 'First Revenue Official'),
                      focusNode: _focusRevOfficial1,
                      attribute: 'revOfficial1',
                      initialValue: revOfficial1,
                      validators: [FormBuilderValidators.required()],
                      onFieldSubmitted: (v) {
                        FocusScope.of(context).requestFocus(_focusRevOfficial2);
                      },
                    ),
                    FormBuilderTextField(
                      decoration: InputDecoration(labelText: 'Second Revenue Official'),
                      focusNode: _focusRevOfficial2,
                      attribute: 'revOfficial2',
                      initialValue: revOfficial2,
                      validators: [FormBuilderValidators.required()],
                      onFieldSubmitted: (v) {
                        FocusScope.of(context).requestFocus(_focusForrestOfficer);
                      },
                    ),
                    FormBuilderTextField(
                      decoration: InputDecoration(labelText: 'Forrest/Wildlife Revenue Official'),
                      focusNode: _focusForrestOfficer,
                      validators: [FormBuilderValidators.required()],
                      attribute: 'forrestOfficial',
                      initialValue: forrestOfficial,
                      onFieldSubmitted: (v) {
                        FocusScope.of(context).requestFocus(_focusSync);
                      },
                    ),
                    Divider(),
                    FormBuilderTextField(
                      decoration: InputDecoration(labelText: 'Sync server'),
                      focusNode: _focusSync,
                      validators: [FormBuilderValidators.url(), FormBuilderValidators.required()],
                      attribute: 'syncServer',
                      initialValue: syncServer,
                      onFieldSubmitted: (v) {
                        FocusScope.of(context).unfocus();
                      },
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  RaisedButton(
                    child: Text("Save"),
                    onPressed: () async {
                      if (_formKey.currentState.saveAndValidate()) {
                        print(_formKey.currentState.value);
                        var prefs = GetIt.I.get<SharedPreferences>();
                        await prefs.setString('operatorName',
                            _formKey.currentState.fields['operator'].currentState.value);
                        await prefs.setString('revOfficial1',
                            _formKey.currentState.fields['revOfficial1'].currentState.value);
                        await prefs.setString('revOfficial2',
                            _formKey.currentState.fields['revOfficial2'].currentState.value);
                        await prefs.setString('forrestOfficial',
                            _formKey.currentState.fields['forrestOfficial'].currentState.value);
                        await prefs.setString('syncServer',
                            _formKey.currentState.fields['syncServer'].currentState.value);
                        await prefs.setInt(
                            'settingsLastUpdated', DateTime.now().millisecondsSinceEpoch);
                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text('Settings updated!'),
                        ));
                      }
                    },
                  ),
                  RaisedButton(
                    child: Text("Reset"),
                    onPressed: () {
                      _formKey.currentState.reset();
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
