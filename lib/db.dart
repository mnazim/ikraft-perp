import 'dart:async';

import 'package:moor/ffi.dart';
import 'package:moor/moor.dart';
import 'package:perp/external_storage.dart';

part 'db.g.dart';

class FieldEntry {
  FieldEntry(this.property, this.structures, this.village);

  final Property property;
  final FutureOr<List<Structure>> structures;
  final FutureOr<Village> village;
  String get villageDisplay => '';

  @override
  String toString() => 'FieldEntry(propertyId: ${property.id})';
}

LazyDatabase _openConnection() {
  // the LazyDatabase util lets us find the right location for the file async.
  return LazyDatabase(() async {
    // put the database file, called db.sqlite here, into the documents folder
    // for your app.
    final dbFile = await ExternalStorage.getDBFile();
    return VmDatabase(dbFile);
  });
}

@UseMoor(include: {'package:perp/tables.moor'})
class PerpDatabase extends _$PerpDatabase {
  PerpDatabase() : super(_openConnection());

  @override
  int get schemaVersion => 1;

  int get secondsSinceEpoch => DateTime.now().millisecondsSinceEpoch ~/ 1000;

  Future<List<Map<String, dynamic>>> allVillages() async {
    var res = (await (select(villages)..orderBy([(t) => OrderingTerm(expression: t.name)])).get())
        .map((rec) => {
              'code': rec.code,
              'name': '${rec.name} (${rec.type.substring(0, 3)}), ${rec.tehsil}',
            })
        .toList();
    return res;
  }

  Future<List<String>> allLandTypes() async {
    return (await select(landTypes).get()).map((rec) => rec.name).toList();
  }

  Future<List<String>> allLandClassifications() async {
    return (await select(landClassifications).get()).map((rec) => rec.name).toList();
  }

  Future<List<String>> allStructureBuildTypes() async {
    return (await select(structureBuildTypes).get()).map((rec) => rec.name).toList();
  }

  Future<List<String>> allStructureClassifications() async {
    return (await select(structureClassifications).get()).map((rec) => rec.name).toList();
  }

  Future<List<String>> allPermissionTypes() async {
    return (await select(structurePermissionTypes).get()).map((rec) => rec.name).toList();
  }

  Stream<Map<String, int>> counts() {
    final all = properties.id.count();
    final entries = properties.operator.count();
    var query = (selectOnly(properties)..addColumns([all, entries])).map((res) {
      return {
        'all': res.read(all).toInt(),
        'entries': res.read(entries).toInt(),
      };
    });
    return query.watch().map((res) => Map<String, int>.from(res.first));
  }

  Stream<Iterable<Property>> propertySearch(
      {List<String> needles = const <String>[], offset = 0, limit = 10}) {
    return (select(properties)
          ..where((t) {
            var exprs = needles.map((n) => t.fullText.contains(n));
            if (exprs.isEmpty) return Constant(true);
            return exprs.reduce((exp1, exp2) => exp1 & exp2);
          })
          ..orderBy([(t) => OrderingTerm.desc(t.modified)]))
        .watch();
  }

  Future<int> syncedCount() async {
    return (await (select(properties)..where((t) => t.syncedOn.isBiggerThan(Constant(0)))).get())
        .length;
  }

  Future<Iterable<Property>> unsyncedProperties() {
    return (select(properties)
          ..where((t) => t.modified.isBiggerThan(Constant(0)) & t.syncedOn.equals(0)))
        .get();
  }

  void markPropertySynced(int id) {
    (update(properties)..where((t) => t.id.equals(id)))
        .write(PropertiesCompanion(syncedOn: Value<int>(secondsSinceEpoch)));
  }

  Future<Village> getVillage(String code) =>
      (select(villages)..where((t) => t.code.equals(code))).getSingle();

  Future<Property> getProperty(int id) async {
    return (select(properties)..where((t) => t.id.equals(id))).getSingle();
  }

  Future<List<Structure>> getPropertyStructures(int propertyId) async {
    return (select(structures)..where((t) => t.propertyId.equals(propertyId))).get();
  }

  Future<Property> addProperty(PropertiesCompanion property) async {
    var ts = secondsSinceEpoch;
    var p = updateFullText(property);
    int id = await into(properties).insert(
      p.copyWith(created: Value<int>(ts), modified: Value<int>(ts)),
    );
    Property saved = await (select(properties)..where((t) => t.id.equals(id))).getSingle();
    print('---> saved $saved');
    return saved;
  }

  Future<Property> updateProperty(PropertiesCompanion property) async {
    var p = updateFullText(property);
    await update(properties).replace(p.copyWith(
      modified: Value<int>(secondsSinceEpoch),
    ));

    Property saved = await (select(properties)..where((t) => t.id.equals(p.id.value))).getSingle();
    print('---> saved $saved');
    return saved;
  }

  PropertiesCompanion updateFullText(PropertiesCompanion p) {
    var text = '${p.owner.value} '
        '${p.parentage.value} '
        '${p.mobileNumber.value} '
        '${p.villageCode.value} '
        '${p.landType.value} '
        '${p.landClassification.value} '
        '${p.parentage.value} ';
    return p.copyWith(fullText: Value<String>(text));
  }

  Future<Structure> addStructure(StructuresCompanion structure) async {
    var ts = secondsSinceEpoch;
    int id = await into(structures).insert(
      structure.copyWith(created: Value<int>(ts), modified: Value<int>(ts)),
    );
    Structure saved = await (select(structures)..where((t) => t.id.equals(id))).getSingle();

    return saved;
  }

  Future<Structure> updateStructure(StructuresCompanion structure) async {
    await update(structures).replace(
      structure.copyWith(modified: Value<int>(secondsSinceEpoch)),
    );
    Structure saved =
        await (select(structures)..where((t) => t.id.equals(structure.id.value))).getSingle();

    return saved;
  }

  Future<int> deleteProperty(int id) async {
    var pDelCount = await (delete(properties)..where((t) => t.id.equals(id))).go();
    var sDelCount = await (delete(structures)..where((t) => t.propertyId.equals(id))).go();
    return pDelCount + sDelCount;
  }

  Future<int> deleteStructure(int id) async {
    return (delete(structures)..where((t) => t.id.equals(id))).go();
  }
}
