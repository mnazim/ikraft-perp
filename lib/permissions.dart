import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:perp/injectables.dart';

class PermissionsService {
  Map<Permission, PermissionStatus> status = {
    Permission.camera: PermissionStatus.undetermined,
    Permission.storage: PermissionStatus.undetermined,
    Permission.location: PermissionStatus.undetermined,
  };

  Future<PermissionsService> init() async {
    status = {
      Permission.camera: await Permission.camera.status,
      Permission.storage: await Permission.storage.status,
      Permission.location: await Permission.location.status,
    };
    return this;
  }

  bool get granted => status.values.every((s) => s == PermissionStatus.granted);

  bool get notGranted => !granted;

  void requestPermissions() async {
    var perms = PermService.state.status.keys.toList();
    status = await perms.request();
  }
}

class PermissionsScreen extends StatelessWidget {
  PermissionsScreen({Key key, this.onChanged}) : super(key: key);

  static final routeName = '/getPerms';
  final VoidCallback onChanged;

  void getPermissions() async {
    var perms = PermService.state.status.keys.toList();
    var status = await perms.request();
    PermService.setState((s) => s.status = status);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Permissions required'),
        ),
        body: Padding(
          padding: const EdgeInsets.only(top: 24, left: 36, right: 36),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                      'This app requires some permissions before it can be used. Grant required permission to use this app.'),
                ),
                _buildStatuses(),
                RaisedButton(
                    child: Text('grant all permissions'),
                    onPressed: () {
                      PermService.setState((s) => s.requestPermissions(), onData: (c, ps) {
                        // if (ps.granted) Navigator.of(c).pop();
                      });
                    }),
              ]),
        ));
  }

  Widget _buildStatuses() {
    var rows = PermService.state.status.entries.map((e) => Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            _buildPermissionStatus(e.value),
            _buildStatusTitle(e.key),
          ],
        ));
    return Container(
      child: Column(children: rows.toList()),
    );
  }

  Widget _buildStatusTitle(Permission perm) {
    var text = perm.toString();
    switch (perm.toString()) {
      case 'Permission.camera':
        text = 'Camera';
        break;
      case 'Permission.storage':
        text = 'Storage';
        break;
      case 'Permission.location':
        text = 'Location';
        break;
      case 'Permission.ignoreBatteryOptimizations':
        text = 'Ignore Battery Optimizations';
        break;
    }
    return Text(
      text,
      style: TextStyle(fontSize: 18),
    );
  }

  Widget _buildPermissionStatus(PermissionStatus status) {
    IconData icon = status == PermissionStatus.granted ? Icons.check : Icons.close;
    return Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: Icon(
        icon,
        size: 24,
      ),
    );
  }
}
