import 'dart:convert';
import 'dart:io';

import 'package:archive/archive_io.dart';
import 'package:uuid/uuid.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:path/path.dart' as path;
import 'package:perp/db.dart';
import 'package:perp/external_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class SyncState {
  PerpDatabase get db => GetIt.I.get<PerpDatabase>();
  List<Property> unsyncedProperties = [];
  int syncedCount;
  String operator;
  String syncServer;
  bool isSyncing = false;
  Directory syncDir;
  Directory photosDir;

  void fetchUnsynced() async {
    syncDir = await ExternalStorage.getSyncDir();
    photosDir = await ExternalStorage.getPhotosDir();
    var p = await db.unsyncedProperties();
    unsyncedProperties = p.toList();
    syncedCount = await db.syncedCount();
    var prefs = GetIt.I.get<SharedPreferences>();
    operator = prefs.getString('operatorName');
    syncServer = prefs.getString('syncServer');
  }

  Directory getPropertySyncDir(int propertyId) {
    var ns = 'perp.ikraftsoft.com/$operator/$propertyId';
    var id = Uuid().v5(Uuid.NAMESPACE_URL, ns);
    var dir = Directory(path.join(syncDir.path, '$id'));
    if (!dir.existsSync()) {
      dir.createSync(recursive: true);
    }
    return dir;
  }

  List<String> copyPhotos(Structure structure, Directory destDir) {
    var filenames = <String>[];

    if (structure.photos?.isNotEmpty ?? false) {
      var photos = List<String>.from(jsonDecode(structure.photos));
      for (var p in photos) {
        var filename = path.basename(p);
        filenames.add(filename);
        var dest = path.join(destDir.path, filename);
        var file = File(path.join(photosDir.path, p));
        file.copySync(dest);
      }
    }
    return filenames;
  }

  Future<int> syncProperty(Property property) async {
    var propSyncDir = getPropertySyncDir(property.id);
    var propJson = property.toJson();
    propJson['structures'] = [];

    var structures = await db.getPropertyStructures(property.id);

    for (var st in structures) {
      var filenames = copyPhotos(st, propSyncDir);
      var json = st.toJson();
      json['photos'] = filenames;
      propJson['structures'].add(json);
    }

    var jsonStr = jsonEncode(propJson);
    var jsonFile = File(path.join(propSyncDir.path, 'data.json'));
    await jsonFile.writeAsString(jsonStr);

    var zip = ZipFileEncoder();
    zip.zipDirectory(propSyncDir);
    propSyncDir.deleteSync(recursive: true);
    var data = FormData.fromMap({
      'zipFile': await MultipartFile.fromFile(zip.zip_path),
    });
    var res = await Dio().post('$syncServer/upload.php', data: data);
    print(
        'Response $res \n Code: ${res.statusCode}\n Message: ${res.statusMessage}\n Data: ${res.data}\n Header: ${res.headers} ');

    if (res.statusCode == 200) {
      db.markPropertySynced(property.id);
      File(zip.zip_path).deleteSync();
      unsyncedProperties.removeWhere((p) => p.id == property.id);
      syncedCount += 1;
    }
    // zip.close();
    return 0;
  }

  Future<void> sync() async {
    var syncer = List.from(unsyncedProperties);
    for (var prop in syncer) {
      await syncProperty(prop);
    }
  }
}

final syncStateRM = RM.inject(() => SyncState());

class SyncScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return syncStateRM.rebuilder(
        () => Scaffold(
              appBar: AppBar(title: Text('Sync')),
              body: _buildBody(),
            ), initState: () {
      syncStateRM.setState((s) => s.fetchUnsynced());
    });
  }

  Widget _buildBody() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _buildCount('Un-synced', syncStateRM.state.unsyncedProperties.length),
              if (syncStateRM.state.isSyncing)
                SizedBox(width: 48, child: LinearProgressIndicator()),
              _buildCount('Synced', syncStateRM.state.syncedCount),
            ],
          ),
          _buildSyncButton(),
        ],
      ),
    );
  }

  Widget _buildCount(String label, int count) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          constraints: BoxConstraints(minWidth: 100),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text('$count', style: TextStyle(fontSize: 40)),
              Text(label),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildSyncButton() {
    return RaisedButton(
      child: Text('Start sync'),
      onPressed: syncStateRM.state.isSyncing || syncStateRM.state.unsyncedProperties.isEmpty
          ? null
          : () async {
              syncStateRM.setState((s) => s.isSyncing = true);
              var syncer = List.from(syncStateRM.state.unsyncedProperties);
              for (var prop in syncer) {
                syncStateRM.setState((s) => s.syncProperty(prop));
                // await Future.delayed(Duration(seconds: 3));
              }
              syncStateRM.setState((s) => s.isSyncing = false);
            },
    );
  }
}
