import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get_it/get_it.dart';
import 'package:perp/constants.dart';
import 'package:perp/field_entry_screen.dart';
import 'package:perp/injectables.dart';
import 'package:perp/location_field.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

final GlobalKey<FormBuilderState> _formKey = GlobalKey<FormBuilderState>();

class PropertyForm extends StatelessWidget {
  final _focusOwner = FocusNode();
  final _focusLocation = FocusNode();
  final _focusParent = FocusNode();
  final _focusMobile = FocusNode();
  final _focusAddress = FocusNode();
  final _focusDetails = FocusNode();
  final _focusKhasra = FocusNode();
  final _focusNala = FocusNode();
  final _focusKanal = FocusNode();
  final _focusMarla = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    final fields = <Widget>[
      _buildLocationField(context),
      _buildManualLocationField(context),
      _buildOwnerField(context),
      _buildParentageField(context),
      _buildPhoneField(context),
      _buildDistanceToNalaField(context),
      _buildAreaFields(context),
      _buildKhasraNumberField(context),
      _buildAddressField(context),
      _buildDetailsField(context),
      _buildVillagesField(context),
      _buildLandTypesField(context),
      _buildLandClassificationField(context),
      _buildSavePropertyButton(context),
    ]
        .map((f) =>
            SliverToBoxAdapter(child: Padding(padding: const EdgeInsets.all(8.0), child: f)))
        .toList();
    return FormBuilder(
      key: _formKey,
      child: CustomScrollView(
        slivers: [
          SliverAppBar(
            title: Text('Property form'),
            floating: true,
          ),
          ...fields
        ],
      ),
    );
  }

  Widget _buildLocationField(BuildContext context) {
    var locationString = entryScreenState.state.property?.location ?? '';

    return LocationField(
      location: entryScreenState.state.jsonToLocation(locationString),
      onChanged: (value) {
        FocusScope.of(context).requestFocus(_focusLocation);
      },
    );
  }

  Widget _buildManualLocationField(BuildContext context) {
    return FormBuilderTextField(
      attribute: 'locationManual',
      initialValue: entryScreenState.state.property?.locationManual ?? '',
      focusNode: _focusLocation,
      decoration: kInputDecoration.copyWith(
        labelText: 'Latitude,Longitude pair',
        prefixIcon: Icon(Icons.pin_drop),
      ),
      autovalidateMode: AutovalidateMode.always,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (v) => FocusScope.of(context).requestFocus(_focusOwner),
    );
  }

  Widget _buildOwnerField(BuildContext context) {
    return FormBuilderTextField(
      attribute: 'owner',
      initialValue: entryScreenState.state.property?.owner ?? null,
      focusNode: _focusOwner,
      decoration: kInputDecoration.copyWith(
        labelText: 'Owner of the property',
        prefixIcon: Icon(Icons.person),
      ),
      validators: [FormBuilderValidators.required()],
      autovalidateMode: AutovalidateMode.always,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (v) => FocusScope.of(context).requestFocus(_focusParent),
      // valueTransformer: (v) => moor.Value<String>(v),
    );
  }

  Widget _buildParentageField(BuildContext context) {
    return FormBuilderTextField(
      decoration: kInputDecoration.copyWith(
        labelText: 'Parentage',
        hintText: "Owner's father or legal guardian.",
        prefixIcon: Icon(Icons.person),
      ),
      attribute: 'parentage',
      initialValue: entryScreenState.state.property?.parentage ?? null,
      focusNode: _focusParent,
      autovalidateMode: AutovalidateMode.always,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (v) => FocusScope.of(context).requestFocus(_focusMobile),
      // valueTransformer: (v) => moor.Value<String>(v),
    );
  }

  Widget _buildPhoneField(BuildContext context) {
    return FormBuilderTextField(
      decoration: kInputDecoration.copyWith(
        labelText: 'Mobile number',
        prefixIcon: Icon(Icons.phone_iphone),
      ),
      attribute: 'mobileNumber',
      initialValue: entryScreenState.state.property?.mobileNumber ?? null,
      focusNode: _focusMobile,
      keyboardType: TextInputType.phone,
      maxLengthEnforced: true,
      validators: [
        FormBuilderValidators.numeric(),
        // ignore: missing_return
        (value) {
          if (value.isNotEmpty && value.length != 10) {
            return 'Must be exactly 10 digits long';
          }
        },
      ],
      autovalidateMode: AutovalidateMode.always,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (v) => FocusScope.of(context).requestFocus(_focusNala),
      // valueTransformer: (v) => moor.Value<String>(v),
    );
  }

  Widget _buildDistanceToNalaField(BuildContext context) {
    return FormBuilderTextField(
      attribute: 'distanceToNala',
      initialValue: entryScreenState.state.property?.distanceToNala?.toString() ?? null,
      decoration: kInputDecoration.copyWith(
        labelText: 'Distance from nala',
        prefixIcon: Icon(Icons.home),
        suffixIcon: Text('meter'),
      ),
      focusNode: _focusNala,
      keyboardType: TextInputType.number,
      onFieldSubmitted: (v) => FocusScope.of(context).requestFocus(_focusKanal),
      textInputAction: TextInputAction.next,
      autovalidateMode: AutovalidateMode.always,
      // valueTransformer: (v) => moor.Value<int>(int.tryParse(v)),
    );
  }

  Widget _buildAreaFields(BuildContext context) {
    return Row(
      children: [
        Flexible(
          child: FormBuilderTextField(
            decoration: kInputDecoration.copyWith(
              labelText: 'Kanals',
              prefixIcon: Icon(Icons.home),
            ),
            focusNode: _focusKanal,
            textInputAction: TextInputAction.next,
            attribute: 'kanals',
            initialValue: entryScreenState.state.property?.kanals?.toString() ?? null,
            keyboardType: TextInputType.number,
            validators: [
              FormBuilderValidators.required(),
              FormBuilderValidators.numeric(),
            ],
            autovalidateMode: AutovalidateMode.always,
            onFieldSubmitted: (v) => FocusScope.of(context).requestFocus(_focusMarla),
            // valueTransformer: (v) => moor.Value<int>(int.tryParse(v)),
          ),
        ),
        SizedBox(width: 8),
        Flexible(
          child: FormBuilderTextField(
            decoration: kInputDecoration.copyWith(
              labelText: 'Marlas',
              prefixIcon: Icon(Icons.home),
            ),
            attribute: 'marlas',
            initialValue: entryScreenState.state.property?.marlas?.toString() ?? null,
            focusNode: _focusMarla,
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.next,
            validators: [
              FormBuilderValidators.numeric(),
              FormBuilderValidators.required(),
            ],
            autovalidateMode: AutovalidateMode.always,
            onFieldSubmitted: (v) => FocusScope.of(context).requestFocus(_focusKhasra),
            // valueTransformer: (v) => moor.Value<int>(int.tryParse(v)),
          ),
        ),
      ],
    );
  }

  Widget _buildKhasraNumberField(BuildContext context) {
    return FormBuilderTextField(
      attribute: 'khasraNumber',
      initialValue: entryScreenState.state.property?.khasraNumber ?? null,
      decoration: kInputDecoration.copyWith(
        labelText: 'Khasra/Compartment Number',
        prefixIcon: Icon(Icons.home),
      ),
      focusNode: _focusKhasra,
      onFieldSubmitted: (v) => FocusScope.of(context).requestFocus(_focusAddress),
      textInputAction: TextInputAction.next,
      validators: [
        FormBuilderValidators.required(),
      ],
      autovalidateMode: AutovalidateMode.always,
      // valueTransformer: (v) => moor.Value<String>(v),
    );
  }

  Widget _buildAddressField(BuildContext context) {
    return FormBuilderTextField(
      maxLines: 3,
      decoration: kInputDecoration.copyWith(
        labelText: 'Address',
        hintText: "Address of the structure in question.",
        prefixIcon: Icon(Icons.home),
      ),
      attribute: 'address',
      initialValue: entryScreenState.state.property?.address ?? null,
      focusNode: _focusAddress,
      autovalidateMode: AutovalidateMode.always,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (v) => FocusScope.of(context).requestFocus(_focusDetails),
      // valueTransformer: (v) => moor.Value<String>(v),
    );
  }

  Widget _buildVillagesField(BuildContext context) {
    var initialValue =
        entryScreenState.state.property?.villageCode ?? entryScreenState.state.selectedVillage;
    return allVillagesRM.whenRebuilder(
      onIdle: () => Text('idle'),
      onError: (e) => Text('$e'),
      onWaiting: () => Container(),
      onData: () {
        var items = allVillagesRM.state
            .map((Map<String, dynamic> e) => DropdownMenuItem(
                child: Text(
                  e['name'],
                  maxLines: 2,
                  overflow: TextOverflow.clip,
                ),
                value: e['code']))
            .toList();
        return FormBuilderDropdown(
          attribute: 'villageCode',
          decoration: kInputDecoration.copyWith(
            labelText: 'Select village',
            prefixIcon: Icon(Icons.filter_alt),
          ),
          validators: [FormBuilderValidators.required()],
          autovalidateMode: AutovalidateMode.always,
          initialValue: initialValue,
          items: items,
          onChanged: (vc) async {
            await GetIt.I.get<SharedPreferences>().setString('villageCode', vc);
          },
        );
      },
    );
  }

  Widget _buildLandTypesField(BuildContext context) {
    return allLandTypesRM.whenRebuilder(
        onWaiting: () => LinearProgressIndicator(),
        onError: (e) => Text('error $e'),
        onIdle: () => Text('idle'),
        onData: () {
          var lt = entryScreenState.state.property?.landType ?? null;
          lt = lt?.isNotEmpty ?? false ? lt : null;
          return FormBuilderDropdown(
            attribute: 'landType',
            initialValue: lt,
            validators: [FormBuilderValidators.required()],
            autovalidateMode: AutovalidateMode.always,
            decoration: kInputDecoration.copyWith(
              labelText: 'Land type',
              prefixIcon: Icon(Icons.home),
            ),
            items: allLandTypesRM.state.map((e) {
              // print('---> $e');
              return DropdownMenuItem(child: Text('$e'), value: e);
            }).toList(),
          );
        });
  }

  Widget _buildLandClassificationField(BuildContext context) {
    return allLandClassifications.whenRebuilder(
        onWaiting: () => LinearProgressIndicator(),
        onError: (e) => Text('error $e'),
        onIdle: () => Text('idle'),
        onData: () {
          var lc = entryScreenState.state.property?.landClassification ?? null;
          lc = lc?.isNotEmpty ?? false ? lc : null;
          return FormBuilderDropdown(
            attribute: 'landClassification',
            initialValue: lc,
            validators: [FormBuilderValidators.required()],
            autovalidateMode: AutovalidateMode.always,
            decoration: kInputDecoration.copyWith(
              labelText: 'Land Classification',
              prefixIcon: Icon(Icons.home),
            ),
            items: allLandClassifications.state.map((e) {
              return DropdownMenuItem(child: Text('$e'), value: e);
            }).toList(),
            onChanged: (v) => FocusScope.of(context).unfocus(),
          );
        });
  }

  Widget _buildDetailsField(BuildContext context) {
    return FormBuilderTextField(
      maxLines: 3,
      decoration: kInputDecoration.copyWith(
        labelText: 'Details',
        hintText: "Any other details",
        prefixIcon: Icon(Icons.home),
      ),
      focusNode: _focusDetails,
      attribute: 'details',
      initialValue: entryScreenState.state.property?.details ?? null,
      autovalidateMode: AutovalidateMode.always,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (v) => FocusScope.of(context).unfocus(),
      // valueTransformer: (v) => moor.Value<String>(v),
    );
  }

  Widget _buildSavePropertyButton(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        RaisedButton(
          color: Colors.red,
          child: Text('Discard Changes'),
          onPressed: () {
            RM.navigate.back();
          },
        ),
        RaisedButton(
          child: Text('Save Changes'),
          onPressed: () {
            if (_formKey.currentState.saveAndValidate()) {
              var gotoFieldEntryScreen = entryScreenState.state.property == null;
              entryScreenState.setState(
                (s) => s.saveProperty(_formKey.currentState.value),
                onData: (context, state) {
                  if (gotoFieldEntryScreen) {
                    RM.navigate.toReplacement(FieldEntryScreen(propertyId: state.property.id));
                  } else {
                    RM.navigate.back();
                  }
                  Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text('Property saved. Enter structure details now.'),
                  ));
                },
                onError: (context, error) {
                  print("---> $error");
                  Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text('Error: Not saved.'),
                  ));
                },
              );
            } else {}
          },
        ),
      ],
    );
  }
}
