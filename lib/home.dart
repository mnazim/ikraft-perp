import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get_it/get_it.dart';
import 'package:perp/_property_form.dart';
import 'package:perp/_shimmer.dart';
import 'package:perp/constants.dart';
import 'package:perp/db.dart';
import 'package:perp/external_storage.dart';
import 'package:perp/field_entry_screen.dart';
import 'package:perp/injectables.dart';
import 'package:perp/settings.dart';
import 'package:perp/sync.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class HomeScreen extends StatefulWidget {
  static final routeName = '/';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  var ok = false;

  @override
  void initState() {
    super.initState();
    scheduleMicrotask(_check);
  }

  void _check() async {
    var perms = await PermService.stateAsync;
    await Future.delayed(Duration(seconds: 1));
    var notGranted = perms.notGranted;
    while (notGranted) {
      await Navigator.of(context).pushNamed('/getPerms');
      notGranted = perms.notGranted;
    }

    await ExternalStorage.getPhotosDir();

    setState(() {
      ok = true;
    });

    var settingsLastUpdated = GetIt.I.get<SharedPreferences>().getInt('settingsLastUpdated');
    var lastUpdated = settingsLastUpdated == null
        ? null
        : DateTime.fromMillisecondsSinceEpoch(settingsLastUpdated);
    var since = lastUpdated != null ? DateTime.now().difference(lastUpdated) : null;
    if (since == null || since.inHours > 10) {
      await Navigator.of(context).pushNamed('/settings');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PERP Field Entry'),
        actions: _buildActions(context),
      ),
      body: ok ? Search() : Container(),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add_circle, size: 32),
          onPressed: () =>
              RM.navigate.to(entryScreenState.rebuilder(() => PropertyForm(), initState: () {
                entryScreenState.setState((s) => s.init(null));
              }))),
    );
  }

  List<Widget> _buildActions(BuildContext context) {
    var settingsButton = IconButton(
        icon: Icon(Icons.settings),
        onPressed: () {
          Navigator.of(context).pushNamed(SettingsScreen.routeName);
        });
    var syncButton = IconButton(
        icon: Icon(Icons.sync),
        onPressed: () {
          RM.navigate.to(SyncScreen());
        });
    return <Widget>[syncButton, settingsButton];
  }
}

class SearchState {
  SearchState({this.keywords = '', this.selectedVillage = '', this.excludeDraftEntries = false});

  String keywords;
  String selectedVillage;
  bool excludeDraftEntries;
  // Iterable<Future<FieldEntry>> entries;
  List<Map<String, dynamic>> allVillages = [];
  Stream<Iterable<Property>> properties;

  @override
  String toString() => 'SearchModel(keywords: $keywords, '
      'selectedVillage: $selectedVillage, '
      'excludeDraftEntries: $excludeDraftEntries, '
      'query: $whereClause';

  void init() {}
  List<String> get whereClause => [
        if (selectedVillage.isNotEmpty) selectedVillage,
        if (keywords.trim().isNotEmpty) ...keywords.trim().split(' ')
      ];

  void setFilters({String kw, String sv}) async {
    print('--- $sv');
    keywords = kw == null ? keywords : kw;
    selectedVillage = sv == null ? selectedVillage : sv;
    await GetIt.I.get<SharedPreferences>().setString('villageCode', selectedVillage);

    final needles = [
      selectedVillage,
      ...keywords.split(' '),
    ];

    properties = GetIt.I.get<PerpDatabase>().propertySearch(needles: needles);
  }
}

final Injected<SearchState> searchStateRM = RM.inject(() => SearchState());

class Search extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return searchStateRM.rebuilder(
      () {
        final Stream<Iterable<Property>> stream = searchStateRM.state.properties;
        return StreamBuilder<Iterable<Property>>(
            stream: stream,
            builder: (context, snapshot) {
              return CustomScrollView(
                slivers: [
                  SliverPersistentHeader(
                    pinned: false,
                    floating: true,
                    delegate: _SearchHeaderDelegate(),
                  ),
                  _buildResults(snapshot),
                ],
              );
            });
      },
      initState: () {},
    );
  }

  Widget _buildResults(AsyncSnapshot<Iterable<Property>> snapshot) {
    if (snapshot.connectionState == ConnectionState.waiting) {
      return SliverToBoxAdapter(child: ShimmerBuilder());
    }
    if (snapshot.data?.isEmpty ?? true) {
      return SliverToBoxAdapter(
          child: Center(
              child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text('Showing 0 results'),
      )));
    }
    return SliverList(
      delegate: SliverChildBuilderDelegate((context, index) {
        var prop = snapshot.data.elementAt(index);
        return InkWell(
          child: PropertyTile(property: prop),
          onTap: () => RM.navigate.to(FieldEntryScreen(propertyId: prop.id)),
        );
      }, childCount: snapshot.data.length),
    );
  }
}

class _SearchHeaderDelegate extends SliverPersistentHeaderDelegate {
  double _scrollAnimationValue(double shrinkOffset) {
    double maxScrollAllowed = maxExtent - minExtent;
    return ((maxScrollAllowed - shrinkOffset) / maxScrollAllowed).clamp(0, 1).toDouble();
  }

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    final double visibleMainHeight = max(maxExtent - shrinkOffset, minExtent);
    final double animationVal = _scrollAnimationValue(shrinkOffset);
    return Container(
      height: visibleMainHeight,
      width: double.infinity,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(color: Colors.green),
          Opacity(
            opacity: animationVal,
            child: Container(color: Colors.orange.shade100),
          ),
          Positioned(
            top: 0.0,
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    _buildVillagesFilter(),
                    _buildTextField(),
                    // _buildDraftsCheckbox(),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  double get maxExtent => minExtent * 3.5;

  @override
  double get minExtent => 56;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) => true;

  Widget _buildVillagesFilter() {
    return allVillagesRM.whenRebuilder(
      onIdle: () => Text('idle'),
      onError: (e) => Text('$e'),
      onWaiting: () => Container(),
      onData: () {
        var items = allVillagesRM.state
            .map((Map<String, dynamic> e) => DropdownMenuItem(
                child: Text(
                  e['name'],
                  maxLines: 2,
                  overflow: TextOverflow.clip,
                ),
                value: e['code']))
            .toList();
        return DropdownButtonFormField(
          decoration: kInputDecoration.copyWith(
            labelText: 'Filter by village',
            prefixIcon: Icon(Icons.filter_alt),
          ),
          items: items,
          onChanged: (value) async {
            searchStateRM.setState((s) => s.setFilters(sv: value));
          },
        );
      },
    );
  }

  Widget _buildTextField() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: TextFormField(
        textInputAction: TextInputAction.search,
        decoration: kInputDecoration.copyWith(
          labelText: 'Filter by keywords',
          helperText: 'Keywords separated by space',
          prefixIcon: Icon(Icons.search),
        ),
        onFieldSubmitted: (v) async {
          searchStateRM.setState((s) => s.setFilters(kw: v));
        },
      ),
    );
  }

  // Widget _buildDraftsCheckbox() {
  //   return Padding(
  //       padding: const EdgeInsets.symmetric(vertical: 8.0),
  //       child: SwitchListTile(
  //         title: Text('Exclude drafts'),
  //         onChanged: (bool value) =>
  //             searchStateRM.setState((s) => s.excludeDraftEntries = !s.excludeDraftEntries),
  //         value: RM.get<SearchState>().state.excludeDraftEntries,
  //       ));
  // }
}
