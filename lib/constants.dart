import 'package:flutter/material.dart';

const InputDecoration kInputDecoration = InputDecoration(
  border: InputBorder.none,
  fillColor: Colors.white,
  filled: true,
);

const kAppTitle = 'PERP';

const kAppDir = 'PERP';

const kVillages = <String>[
  'Veersiran, Sallar',
  'Khelan Gojran, Sallar',
  'Mavora, Sallar',
  'Yenned, Sallar',
  'Amad wagad, Pahalgam',
  'Hardu Kitchroo, Pahalgam',
  'Sarchin, Pahalgam',
  'Aru, Pahalgam',
  'Nunwanan, Pahalgam',
  'Pahalgam, Pahalgam',
  'Lidroo, Pahalgam',
  "Mamal, Pahalgam",
  'Botkote, Pahalgam',
  'Mondlan, Pahalgam',
  'Freslum, Pahalgam',
  'Laripora, Pahalgam',
];

const kLandClassifications = [
  "Government",
  "Milkiyat",
  "Forest",
  "Dafa 5",
  "Shamilat",
  "Gass charai",
  "Other"
];

const kLandTypes = ["Agriculture", "Bagati", "Banjar", "Abaad", "Gair abad", "Other"];

const kStuctureBuildTypes = ["Pucca", "Kucha", "Cow shed", "Shed", "Other"];

const kStructureClassifications = [
  "Residential",
  "Commercial",
  "Government",
  "Religious",
  "Other"
];

const kStructurePermissionType = ["No permission", "PDA", "MC", "Other"];
