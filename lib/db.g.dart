// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'db.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class Village extends DataClass implements Insertable<Village> {
  final String code;
  final String name;
  final String type;
  final String tehsil;
  Village(
      {@required this.code,
      @required this.name,
      @required this.type,
      @required this.tehsil});
  factory Village.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    return Village(
      code: stringType.mapFromDatabaseResponse(data['${effectivePrefix}code']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      type: stringType.mapFromDatabaseResponse(data['${effectivePrefix}type']),
      tehsil:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}tehsil']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || code != null) {
      map['code'] = Variable<String>(code);
    }
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String>(name);
    }
    if (!nullToAbsent || type != null) {
      map['type'] = Variable<String>(type);
    }
    if (!nullToAbsent || tehsil != null) {
      map['tehsil'] = Variable<String>(tehsil);
    }
    return map;
  }

  VillagesCompanion toCompanion(bool nullToAbsent) {
    return VillagesCompanion(
      code: code == null && nullToAbsent ? const Value.absent() : Value(code),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      type: type == null && nullToAbsent ? const Value.absent() : Value(type),
      tehsil:
          tehsil == null && nullToAbsent ? const Value.absent() : Value(tehsil),
    );
  }

  factory Village.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Village(
      code: serializer.fromJson<String>(json['code']),
      name: serializer.fromJson<String>(json['name']),
      type: serializer.fromJson<String>(json['type']),
      tehsil: serializer.fromJson<String>(json['tehsil']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'code': serializer.toJson<String>(code),
      'name': serializer.toJson<String>(name),
      'type': serializer.toJson<String>(type),
      'tehsil': serializer.toJson<String>(tehsil),
    };
  }

  Village copyWith({String code, String name, String type, String tehsil}) =>
      Village(
        code: code ?? this.code,
        name: name ?? this.name,
        type: type ?? this.type,
        tehsil: tehsil ?? this.tehsil,
      );
  @override
  String toString() {
    return (StringBuffer('Village(')
          ..write('code: $code, ')
          ..write('name: $name, ')
          ..write('type: $type, ')
          ..write('tehsil: $tehsil')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(code.hashCode,
      $mrjc(name.hashCode, $mrjc(type.hashCode, tehsil.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Village &&
          other.code == this.code &&
          other.name == this.name &&
          other.type == this.type &&
          other.tehsil == this.tehsil);
}

class VillagesCompanion extends UpdateCompanion<Village> {
  final Value<String> code;
  final Value<String> name;
  final Value<String> type;
  final Value<String> tehsil;
  const VillagesCompanion({
    this.code = const Value.absent(),
    this.name = const Value.absent(),
    this.type = const Value.absent(),
    this.tehsil = const Value.absent(),
  });
  VillagesCompanion.insert({
    @required String code,
    @required String name,
    @required String type,
    @required String tehsil,
  })  : code = Value(code),
        name = Value(name),
        type = Value(type),
        tehsil = Value(tehsil);
  static Insertable<Village> custom({
    Expression<String> code,
    Expression<String> name,
    Expression<String> type,
    Expression<String> tehsil,
  }) {
    return RawValuesInsertable({
      if (code != null) 'code': code,
      if (name != null) 'name': name,
      if (type != null) 'type': type,
      if (tehsil != null) 'tehsil': tehsil,
    });
  }

  VillagesCompanion copyWith(
      {Value<String> code,
      Value<String> name,
      Value<String> type,
      Value<String> tehsil}) {
    return VillagesCompanion(
      code: code ?? this.code,
      name: name ?? this.name,
      type: type ?? this.type,
      tehsil: tehsil ?? this.tehsil,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (code.present) {
      map['code'] = Variable<String>(code.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (type.present) {
      map['type'] = Variable<String>(type.value);
    }
    if (tehsil.present) {
      map['tehsil'] = Variable<String>(tehsil.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('VillagesCompanion(')
          ..write('code: $code, ')
          ..write('name: $name, ')
          ..write('type: $type, ')
          ..write('tehsil: $tehsil')
          ..write(')'))
        .toString();
  }
}

class Villages extends Table with TableInfo<Villages, Village> {
  final GeneratedDatabase _db;
  final String _alias;
  Villages(this._db, [this._alias]);
  final VerificationMeta _codeMeta = const VerificationMeta('code');
  GeneratedTextColumn _code;
  GeneratedTextColumn get code => _code ??= _constructCode();
  GeneratedTextColumn _constructCode() {
    return GeneratedTextColumn('code', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn('name', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _typeMeta = const VerificationMeta('type');
  GeneratedTextColumn _type;
  GeneratedTextColumn get type => _type ??= _constructType();
  GeneratedTextColumn _constructType() {
    return GeneratedTextColumn('type', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _tehsilMeta = const VerificationMeta('tehsil');
  GeneratedTextColumn _tehsil;
  GeneratedTextColumn get tehsil => _tehsil ??= _constructTehsil();
  GeneratedTextColumn _constructTehsil() {
    return GeneratedTextColumn('tehsil', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  @override
  List<GeneratedColumn> get $columns => [code, name, type, tehsil];
  @override
  Villages get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'villages';
  @override
  final String actualTableName = 'villages';
  @override
  VerificationContext validateIntegrity(Insertable<Village> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('code')) {
      context.handle(
          _codeMeta, code.isAcceptableOrUnknown(data['code'], _codeMeta));
    } else if (isInserting) {
      context.missing(_codeMeta);
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name'], _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('type')) {
      context.handle(
          _typeMeta, type.isAcceptableOrUnknown(data['type'], _typeMeta));
    } else if (isInserting) {
      context.missing(_typeMeta);
    }
    if (data.containsKey('tehsil')) {
      context.handle(_tehsilMeta,
          tehsil.isAcceptableOrUnknown(data['tehsil'], _tehsilMeta));
    } else if (isInserting) {
      context.missing(_tehsilMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {code};
  @override
  Village map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Village.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Villages createAlias(String alias) {
    return Villages(_db, alias);
  }

  @override
  List<String> get customConstraints =>
      const ['UNIQUE("name","type","tehsil")', 'PRIMARY KEY("code")'];
  @override
  bool get dontWriteConstraints => true;
}

class Property extends DataClass implements Insertable<Property> {
  final int id;
  final String owner;
  final String parentage;
  final String mobileNumber;
  final String location;
  final String locationManual;
  final String address;
  final String district;
  final String villageCode;
  final String landType;
  final String landClassification;
  final int distanceToNala;
  final String khasraNumber;
  final int kanals;
  final double marlas;
  final String details;
  final String operator;
  final String revOfficial1;
  final String revOfficial2;
  final String forrestOfficial;
  final int created;
  final int modified;
  final String fullText;
  final int syncedOn;
  Property(
      {@required this.id,
      @required this.owner,
      this.parentage,
      this.mobileNumber,
      @required this.location,
      this.locationManual,
      this.address,
      @required this.district,
      @required this.villageCode,
      @required this.landType,
      @required this.landClassification,
      this.distanceToNala,
      @required this.khasraNumber,
      @required this.kanals,
      @required this.marlas,
      this.details,
      @required this.operator,
      @required this.revOfficial1,
      @required this.revOfficial2,
      @required this.forrestOfficial,
      @required this.created,
      @required this.modified,
      @required this.fullText,
      @required this.syncedOn});
  factory Property.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final doubleType = db.typeSystem.forDartType<double>();
    return Property(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      owner:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}owner']),
      parentage: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}parentage']),
      mobileNumber: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}mobile_number']),
      location: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}location']),
      locationManual: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}location_manual']),
      address:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}address']),
      district: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}district']),
      villageCode: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}village_code']),
      landType: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}land_type']),
      landClassification: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}land_classification']),
      distanceToNala: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}distance_to_nala']),
      khasraNumber: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}khasra_number']),
      kanals: intType.mapFromDatabaseResponse(data['${effectivePrefix}kanals']),
      marlas:
          doubleType.mapFromDatabaseResponse(data['${effectivePrefix}marlas']),
      details:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}details']),
      operator: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}operator']),
      revOfficial1: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}rev_official1']),
      revOfficial2: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}rev_official2']),
      forrestOfficial: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}forrest_official']),
      created:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}created']),
      modified:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}modified']),
      fullText: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}full_text']),
      syncedOn:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}synced_on']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || owner != null) {
      map['owner'] = Variable<String>(owner);
    }
    if (!nullToAbsent || parentage != null) {
      map['parentage'] = Variable<String>(parentage);
    }
    if (!nullToAbsent || mobileNumber != null) {
      map['mobile_number'] = Variable<String>(mobileNumber);
    }
    if (!nullToAbsent || location != null) {
      map['location'] = Variable<String>(location);
    }
    if (!nullToAbsent || locationManual != null) {
      map['location_manual'] = Variable<String>(locationManual);
    }
    if (!nullToAbsent || address != null) {
      map['address'] = Variable<String>(address);
    }
    if (!nullToAbsent || district != null) {
      map['district'] = Variable<String>(district);
    }
    if (!nullToAbsent || villageCode != null) {
      map['village_code'] = Variable<String>(villageCode);
    }
    if (!nullToAbsent || landType != null) {
      map['land_type'] = Variable<String>(landType);
    }
    if (!nullToAbsent || landClassification != null) {
      map['land_classification'] = Variable<String>(landClassification);
    }
    if (!nullToAbsent || distanceToNala != null) {
      map['distance_to_nala'] = Variable<int>(distanceToNala);
    }
    if (!nullToAbsent || khasraNumber != null) {
      map['khasra_number'] = Variable<String>(khasraNumber);
    }
    if (!nullToAbsent || kanals != null) {
      map['kanals'] = Variable<int>(kanals);
    }
    if (!nullToAbsent || marlas != null) {
      map['marlas'] = Variable<double>(marlas);
    }
    if (!nullToAbsent || details != null) {
      map['details'] = Variable<String>(details);
    }
    if (!nullToAbsent || operator != null) {
      map['operator'] = Variable<String>(operator);
    }
    if (!nullToAbsent || revOfficial1 != null) {
      map['rev_official1'] = Variable<String>(revOfficial1);
    }
    if (!nullToAbsent || revOfficial2 != null) {
      map['rev_official2'] = Variable<String>(revOfficial2);
    }
    if (!nullToAbsent || forrestOfficial != null) {
      map['forrest_official'] = Variable<String>(forrestOfficial);
    }
    if (!nullToAbsent || created != null) {
      map['created'] = Variable<int>(created);
    }
    if (!nullToAbsent || modified != null) {
      map['modified'] = Variable<int>(modified);
    }
    if (!nullToAbsent || fullText != null) {
      map['full_text'] = Variable<String>(fullText);
    }
    if (!nullToAbsent || syncedOn != null) {
      map['synced_on'] = Variable<int>(syncedOn);
    }
    return map;
  }

  PropertiesCompanion toCompanion(bool nullToAbsent) {
    return PropertiesCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      owner:
          owner == null && nullToAbsent ? const Value.absent() : Value(owner),
      parentage: parentage == null && nullToAbsent
          ? const Value.absent()
          : Value(parentage),
      mobileNumber: mobileNumber == null && nullToAbsent
          ? const Value.absent()
          : Value(mobileNumber),
      location: location == null && nullToAbsent
          ? const Value.absent()
          : Value(location),
      locationManual: locationManual == null && nullToAbsent
          ? const Value.absent()
          : Value(locationManual),
      address: address == null && nullToAbsent
          ? const Value.absent()
          : Value(address),
      district: district == null && nullToAbsent
          ? const Value.absent()
          : Value(district),
      villageCode: villageCode == null && nullToAbsent
          ? const Value.absent()
          : Value(villageCode),
      landType: landType == null && nullToAbsent
          ? const Value.absent()
          : Value(landType),
      landClassification: landClassification == null && nullToAbsent
          ? const Value.absent()
          : Value(landClassification),
      distanceToNala: distanceToNala == null && nullToAbsent
          ? const Value.absent()
          : Value(distanceToNala),
      khasraNumber: khasraNumber == null && nullToAbsent
          ? const Value.absent()
          : Value(khasraNumber),
      kanals:
          kanals == null && nullToAbsent ? const Value.absent() : Value(kanals),
      marlas:
          marlas == null && nullToAbsent ? const Value.absent() : Value(marlas),
      details: details == null && nullToAbsent
          ? const Value.absent()
          : Value(details),
      operator: operator == null && nullToAbsent
          ? const Value.absent()
          : Value(operator),
      revOfficial1: revOfficial1 == null && nullToAbsent
          ? const Value.absent()
          : Value(revOfficial1),
      revOfficial2: revOfficial2 == null && nullToAbsent
          ? const Value.absent()
          : Value(revOfficial2),
      forrestOfficial: forrestOfficial == null && nullToAbsent
          ? const Value.absent()
          : Value(forrestOfficial),
      created: created == null && nullToAbsent
          ? const Value.absent()
          : Value(created),
      modified: modified == null && nullToAbsent
          ? const Value.absent()
          : Value(modified),
      fullText: fullText == null && nullToAbsent
          ? const Value.absent()
          : Value(fullText),
      syncedOn: syncedOn == null && nullToAbsent
          ? const Value.absent()
          : Value(syncedOn),
    );
  }

  factory Property.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Property(
      id: serializer.fromJson<int>(json['id']),
      owner: serializer.fromJson<String>(json['owner']),
      parentage: serializer.fromJson<String>(json['parentage']),
      mobileNumber: serializer.fromJson<String>(json['mobile_number']),
      location: serializer.fromJson<String>(json['location']),
      locationManual: serializer.fromJson<String>(json['location_manual']),
      address: serializer.fromJson<String>(json['address']),
      district: serializer.fromJson<String>(json['district']),
      villageCode: serializer.fromJson<String>(json['village_code']),
      landType: serializer.fromJson<String>(json['land_type']),
      landClassification:
          serializer.fromJson<String>(json['land_classification']),
      distanceToNala: serializer.fromJson<int>(json['distance_to_nala']),
      khasraNumber: serializer.fromJson<String>(json['khasra_number']),
      kanals: serializer.fromJson<int>(json['kanals']),
      marlas: serializer.fromJson<double>(json['marlas']),
      details: serializer.fromJson<String>(json['details']),
      operator: serializer.fromJson<String>(json['operator']),
      revOfficial1: serializer.fromJson<String>(json['rev_official1']),
      revOfficial2: serializer.fromJson<String>(json['rev_official2']),
      forrestOfficial: serializer.fromJson<String>(json['forrest_official']),
      created: serializer.fromJson<int>(json['created']),
      modified: serializer.fromJson<int>(json['modified']),
      fullText: serializer.fromJson<String>(json['full_text']),
      syncedOn: serializer.fromJson<int>(json['synced_on']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'owner': serializer.toJson<String>(owner),
      'parentage': serializer.toJson<String>(parentage),
      'mobile_number': serializer.toJson<String>(mobileNumber),
      'location': serializer.toJson<String>(location),
      'location_manual': serializer.toJson<String>(locationManual),
      'address': serializer.toJson<String>(address),
      'district': serializer.toJson<String>(district),
      'village_code': serializer.toJson<String>(villageCode),
      'land_type': serializer.toJson<String>(landType),
      'land_classification': serializer.toJson<String>(landClassification),
      'distance_to_nala': serializer.toJson<int>(distanceToNala),
      'khasra_number': serializer.toJson<String>(khasraNumber),
      'kanals': serializer.toJson<int>(kanals),
      'marlas': serializer.toJson<double>(marlas),
      'details': serializer.toJson<String>(details),
      'operator': serializer.toJson<String>(operator),
      'rev_official1': serializer.toJson<String>(revOfficial1),
      'rev_official2': serializer.toJson<String>(revOfficial2),
      'forrest_official': serializer.toJson<String>(forrestOfficial),
      'created': serializer.toJson<int>(created),
      'modified': serializer.toJson<int>(modified),
      'full_text': serializer.toJson<String>(fullText),
      'synced_on': serializer.toJson<int>(syncedOn),
    };
  }

  Property copyWith(
          {int id,
          String owner,
          String parentage,
          String mobileNumber,
          String location,
          String locationManual,
          String address,
          String district,
          String villageCode,
          String landType,
          String landClassification,
          int distanceToNala,
          String khasraNumber,
          int kanals,
          double marlas,
          String details,
          String operator,
          String revOfficial1,
          String revOfficial2,
          String forrestOfficial,
          int created,
          int modified,
          String fullText,
          int syncedOn}) =>
      Property(
        id: id ?? this.id,
        owner: owner ?? this.owner,
        parentage: parentage ?? this.parentage,
        mobileNumber: mobileNumber ?? this.mobileNumber,
        location: location ?? this.location,
        locationManual: locationManual ?? this.locationManual,
        address: address ?? this.address,
        district: district ?? this.district,
        villageCode: villageCode ?? this.villageCode,
        landType: landType ?? this.landType,
        landClassification: landClassification ?? this.landClassification,
        distanceToNala: distanceToNala ?? this.distanceToNala,
        khasraNumber: khasraNumber ?? this.khasraNumber,
        kanals: kanals ?? this.kanals,
        marlas: marlas ?? this.marlas,
        details: details ?? this.details,
        operator: operator ?? this.operator,
        revOfficial1: revOfficial1 ?? this.revOfficial1,
        revOfficial2: revOfficial2 ?? this.revOfficial2,
        forrestOfficial: forrestOfficial ?? this.forrestOfficial,
        created: created ?? this.created,
        modified: modified ?? this.modified,
        fullText: fullText ?? this.fullText,
        syncedOn: syncedOn ?? this.syncedOn,
      );
  @override
  String toString() {
    return (StringBuffer('Property(')
          ..write('id: $id, ')
          ..write('owner: $owner, ')
          ..write('parentage: $parentage, ')
          ..write('mobileNumber: $mobileNumber, ')
          ..write('location: $location, ')
          ..write('locationManual: $locationManual, ')
          ..write('address: $address, ')
          ..write('district: $district, ')
          ..write('villageCode: $villageCode, ')
          ..write('landType: $landType, ')
          ..write('landClassification: $landClassification, ')
          ..write('distanceToNala: $distanceToNala, ')
          ..write('khasraNumber: $khasraNumber, ')
          ..write('kanals: $kanals, ')
          ..write('marlas: $marlas, ')
          ..write('details: $details, ')
          ..write('operator: $operator, ')
          ..write('revOfficial1: $revOfficial1, ')
          ..write('revOfficial2: $revOfficial2, ')
          ..write('forrestOfficial: $forrestOfficial, ')
          ..write('created: $created, ')
          ..write('modified: $modified, ')
          ..write('fullText: $fullText, ')
          ..write('syncedOn: $syncedOn')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          owner.hashCode,
          $mrjc(
              parentage.hashCode,
              $mrjc(
                  mobileNumber.hashCode,
                  $mrjc(
                      location.hashCode,
                      $mrjc(
                          locationManual.hashCode,
                          $mrjc(
                              address.hashCode,
                              $mrjc(
                                  district.hashCode,
                                  $mrjc(
                                      villageCode.hashCode,
                                      $mrjc(
                                          landType.hashCode,
                                          $mrjc(
                                              landClassification.hashCode,
                                              $mrjc(
                                                  distanceToNala.hashCode,
                                                  $mrjc(
                                                      khasraNumber.hashCode,
                                                      $mrjc(
                                                          kanals.hashCode,
                                                          $mrjc(
                                                              marlas.hashCode,
                                                              $mrjc(
                                                                  details
                                                                      .hashCode,
                                                                  $mrjc(
                                                                      operator
                                                                          .hashCode,
                                                                      $mrjc(
                                                                          revOfficial1
                                                                              .hashCode,
                                                                          $mrjc(
                                                                              revOfficial2.hashCode,
                                                                              $mrjc(forrestOfficial.hashCode, $mrjc(created.hashCode, $mrjc(modified.hashCode, $mrjc(fullText.hashCode, syncedOn.hashCode))))))))))))))))))))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Property &&
          other.id == this.id &&
          other.owner == this.owner &&
          other.parentage == this.parentage &&
          other.mobileNumber == this.mobileNumber &&
          other.location == this.location &&
          other.locationManual == this.locationManual &&
          other.address == this.address &&
          other.district == this.district &&
          other.villageCode == this.villageCode &&
          other.landType == this.landType &&
          other.landClassification == this.landClassification &&
          other.distanceToNala == this.distanceToNala &&
          other.khasraNumber == this.khasraNumber &&
          other.kanals == this.kanals &&
          other.marlas == this.marlas &&
          other.details == this.details &&
          other.operator == this.operator &&
          other.revOfficial1 == this.revOfficial1 &&
          other.revOfficial2 == this.revOfficial2 &&
          other.forrestOfficial == this.forrestOfficial &&
          other.created == this.created &&
          other.modified == this.modified &&
          other.fullText == this.fullText &&
          other.syncedOn == this.syncedOn);
}

class PropertiesCompanion extends UpdateCompanion<Property> {
  final Value<int> id;
  final Value<String> owner;
  final Value<String> parentage;
  final Value<String> mobileNumber;
  final Value<String> location;
  final Value<String> locationManual;
  final Value<String> address;
  final Value<String> district;
  final Value<String> villageCode;
  final Value<String> landType;
  final Value<String> landClassification;
  final Value<int> distanceToNala;
  final Value<String> khasraNumber;
  final Value<int> kanals;
  final Value<double> marlas;
  final Value<String> details;
  final Value<String> operator;
  final Value<String> revOfficial1;
  final Value<String> revOfficial2;
  final Value<String> forrestOfficial;
  final Value<int> created;
  final Value<int> modified;
  final Value<String> fullText;
  final Value<int> syncedOn;
  const PropertiesCompanion({
    this.id = const Value.absent(),
    this.owner = const Value.absent(),
    this.parentage = const Value.absent(),
    this.mobileNumber = const Value.absent(),
    this.location = const Value.absent(),
    this.locationManual = const Value.absent(),
    this.address = const Value.absent(),
    this.district = const Value.absent(),
    this.villageCode = const Value.absent(),
    this.landType = const Value.absent(),
    this.landClassification = const Value.absent(),
    this.distanceToNala = const Value.absent(),
    this.khasraNumber = const Value.absent(),
    this.kanals = const Value.absent(),
    this.marlas = const Value.absent(),
    this.details = const Value.absent(),
    this.operator = const Value.absent(),
    this.revOfficial1 = const Value.absent(),
    this.revOfficial2 = const Value.absent(),
    this.forrestOfficial = const Value.absent(),
    this.created = const Value.absent(),
    this.modified = const Value.absent(),
    this.fullText = const Value.absent(),
    this.syncedOn = const Value.absent(),
  });
  PropertiesCompanion.insert({
    this.id = const Value.absent(),
    @required String owner,
    this.parentage = const Value.absent(),
    this.mobileNumber = const Value.absent(),
    @required String location,
    this.locationManual = const Value.absent(),
    this.address = const Value.absent(),
    this.district = const Value.absent(),
    @required String villageCode,
    @required String landType,
    @required String landClassification,
    this.distanceToNala = const Value.absent(),
    @required String khasraNumber,
    @required int kanals,
    @required double marlas,
    this.details = const Value.absent(),
    @required String operator,
    @required String revOfficial1,
    @required String revOfficial2,
    @required String forrestOfficial,
    @required int created,
    @required int modified,
    @required String fullText,
    this.syncedOn = const Value.absent(),
  })  : owner = Value(owner),
        location = Value(location),
        villageCode = Value(villageCode),
        landType = Value(landType),
        landClassification = Value(landClassification),
        khasraNumber = Value(khasraNumber),
        kanals = Value(kanals),
        marlas = Value(marlas),
        operator = Value(operator),
        revOfficial1 = Value(revOfficial1),
        revOfficial2 = Value(revOfficial2),
        forrestOfficial = Value(forrestOfficial),
        created = Value(created),
        modified = Value(modified),
        fullText = Value(fullText);
  static Insertable<Property> custom({
    Expression<int> id,
    Expression<String> owner,
    Expression<String> parentage,
    Expression<String> mobileNumber,
    Expression<String> location,
    Expression<String> locationManual,
    Expression<String> address,
    Expression<String> district,
    Expression<String> villageCode,
    Expression<String> landType,
    Expression<String> landClassification,
    Expression<int> distanceToNala,
    Expression<String> khasraNumber,
    Expression<int> kanals,
    Expression<double> marlas,
    Expression<String> details,
    Expression<String> operator,
    Expression<String> revOfficial1,
    Expression<String> revOfficial2,
    Expression<String> forrestOfficial,
    Expression<int> created,
    Expression<int> modified,
    Expression<String> fullText,
    Expression<int> syncedOn,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (owner != null) 'owner': owner,
      if (parentage != null) 'parentage': parentage,
      if (mobileNumber != null) 'mobile_number': mobileNumber,
      if (location != null) 'location': location,
      if (locationManual != null) 'location_manual': locationManual,
      if (address != null) 'address': address,
      if (district != null) 'district': district,
      if (villageCode != null) 'village_code': villageCode,
      if (landType != null) 'land_type': landType,
      if (landClassification != null) 'land_classification': landClassification,
      if (distanceToNala != null) 'distance_to_nala': distanceToNala,
      if (khasraNumber != null) 'khasra_number': khasraNumber,
      if (kanals != null) 'kanals': kanals,
      if (marlas != null) 'marlas': marlas,
      if (details != null) 'details': details,
      if (operator != null) 'operator': operator,
      if (revOfficial1 != null) 'rev_official1': revOfficial1,
      if (revOfficial2 != null) 'rev_official2': revOfficial2,
      if (forrestOfficial != null) 'forrest_official': forrestOfficial,
      if (created != null) 'created': created,
      if (modified != null) 'modified': modified,
      if (fullText != null) 'full_text': fullText,
      if (syncedOn != null) 'synced_on': syncedOn,
    });
  }

  PropertiesCompanion copyWith(
      {Value<int> id,
      Value<String> owner,
      Value<String> parentage,
      Value<String> mobileNumber,
      Value<String> location,
      Value<String> locationManual,
      Value<String> address,
      Value<String> district,
      Value<String> villageCode,
      Value<String> landType,
      Value<String> landClassification,
      Value<int> distanceToNala,
      Value<String> khasraNumber,
      Value<int> kanals,
      Value<double> marlas,
      Value<String> details,
      Value<String> operator,
      Value<String> revOfficial1,
      Value<String> revOfficial2,
      Value<String> forrestOfficial,
      Value<int> created,
      Value<int> modified,
      Value<String> fullText,
      Value<int> syncedOn}) {
    return PropertiesCompanion(
      id: id ?? this.id,
      owner: owner ?? this.owner,
      parentage: parentage ?? this.parentage,
      mobileNumber: mobileNumber ?? this.mobileNumber,
      location: location ?? this.location,
      locationManual: locationManual ?? this.locationManual,
      address: address ?? this.address,
      district: district ?? this.district,
      villageCode: villageCode ?? this.villageCode,
      landType: landType ?? this.landType,
      landClassification: landClassification ?? this.landClassification,
      distanceToNala: distanceToNala ?? this.distanceToNala,
      khasraNumber: khasraNumber ?? this.khasraNumber,
      kanals: kanals ?? this.kanals,
      marlas: marlas ?? this.marlas,
      details: details ?? this.details,
      operator: operator ?? this.operator,
      revOfficial1: revOfficial1 ?? this.revOfficial1,
      revOfficial2: revOfficial2 ?? this.revOfficial2,
      forrestOfficial: forrestOfficial ?? this.forrestOfficial,
      created: created ?? this.created,
      modified: modified ?? this.modified,
      fullText: fullText ?? this.fullText,
      syncedOn: syncedOn ?? this.syncedOn,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (owner.present) {
      map['owner'] = Variable<String>(owner.value);
    }
    if (parentage.present) {
      map['parentage'] = Variable<String>(parentage.value);
    }
    if (mobileNumber.present) {
      map['mobile_number'] = Variable<String>(mobileNumber.value);
    }
    if (location.present) {
      map['location'] = Variable<String>(location.value);
    }
    if (locationManual.present) {
      map['location_manual'] = Variable<String>(locationManual.value);
    }
    if (address.present) {
      map['address'] = Variable<String>(address.value);
    }
    if (district.present) {
      map['district'] = Variable<String>(district.value);
    }
    if (villageCode.present) {
      map['village_code'] = Variable<String>(villageCode.value);
    }
    if (landType.present) {
      map['land_type'] = Variable<String>(landType.value);
    }
    if (landClassification.present) {
      map['land_classification'] = Variable<String>(landClassification.value);
    }
    if (distanceToNala.present) {
      map['distance_to_nala'] = Variable<int>(distanceToNala.value);
    }
    if (khasraNumber.present) {
      map['khasra_number'] = Variable<String>(khasraNumber.value);
    }
    if (kanals.present) {
      map['kanals'] = Variable<int>(kanals.value);
    }
    if (marlas.present) {
      map['marlas'] = Variable<double>(marlas.value);
    }
    if (details.present) {
      map['details'] = Variable<String>(details.value);
    }
    if (operator.present) {
      map['operator'] = Variable<String>(operator.value);
    }
    if (revOfficial1.present) {
      map['rev_official1'] = Variable<String>(revOfficial1.value);
    }
    if (revOfficial2.present) {
      map['rev_official2'] = Variable<String>(revOfficial2.value);
    }
    if (forrestOfficial.present) {
      map['forrest_official'] = Variable<String>(forrestOfficial.value);
    }
    if (created.present) {
      map['created'] = Variable<int>(created.value);
    }
    if (modified.present) {
      map['modified'] = Variable<int>(modified.value);
    }
    if (fullText.present) {
      map['full_text'] = Variable<String>(fullText.value);
    }
    if (syncedOn.present) {
      map['synced_on'] = Variable<int>(syncedOn.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('PropertiesCompanion(')
          ..write('id: $id, ')
          ..write('owner: $owner, ')
          ..write('parentage: $parentage, ')
          ..write('mobileNumber: $mobileNumber, ')
          ..write('location: $location, ')
          ..write('locationManual: $locationManual, ')
          ..write('address: $address, ')
          ..write('district: $district, ')
          ..write('villageCode: $villageCode, ')
          ..write('landType: $landType, ')
          ..write('landClassification: $landClassification, ')
          ..write('distanceToNala: $distanceToNala, ')
          ..write('khasraNumber: $khasraNumber, ')
          ..write('kanals: $kanals, ')
          ..write('marlas: $marlas, ')
          ..write('details: $details, ')
          ..write('operator: $operator, ')
          ..write('revOfficial1: $revOfficial1, ')
          ..write('revOfficial2: $revOfficial2, ')
          ..write('forrestOfficial: $forrestOfficial, ')
          ..write('created: $created, ')
          ..write('modified: $modified, ')
          ..write('fullText: $fullText, ')
          ..write('syncedOn: $syncedOn')
          ..write(')'))
        .toString();
  }
}

class Properties extends Table with TableInfo<Properties, Property> {
  final GeneratedDatabase _db;
  final String _alias;
  Properties(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        declaredAsPrimaryKey: true,
        hasAutoIncrement: true,
        $customConstraints: 'NOT NULL PRIMARY KEY AUTOINCREMENT');
  }

  final VerificationMeta _ownerMeta = const VerificationMeta('owner');
  GeneratedTextColumn _owner;
  GeneratedTextColumn get owner => _owner ??= _constructOwner();
  GeneratedTextColumn _constructOwner() {
    return GeneratedTextColumn('owner', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _parentageMeta = const VerificationMeta('parentage');
  GeneratedTextColumn _parentage;
  GeneratedTextColumn get parentage => _parentage ??= _constructParentage();
  GeneratedTextColumn _constructParentage() {
    return GeneratedTextColumn('parentage', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _mobileNumberMeta =
      const VerificationMeta('mobileNumber');
  GeneratedTextColumn _mobileNumber;
  GeneratedTextColumn get mobileNumber =>
      _mobileNumber ??= _constructMobileNumber();
  GeneratedTextColumn _constructMobileNumber() {
    return GeneratedTextColumn('mobile_number', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _locationMeta = const VerificationMeta('location');
  GeneratedTextColumn _location;
  GeneratedTextColumn get location => _location ??= _constructLocation();
  GeneratedTextColumn _constructLocation() {
    return GeneratedTextColumn('location', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _locationManualMeta =
      const VerificationMeta('locationManual');
  GeneratedTextColumn _locationManual;
  GeneratedTextColumn get locationManual =>
      _locationManual ??= _constructLocationManual();
  GeneratedTextColumn _constructLocationManual() {
    return GeneratedTextColumn('location_manual', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _addressMeta = const VerificationMeta('address');
  GeneratedTextColumn _address;
  GeneratedTextColumn get address => _address ??= _constructAddress();
  GeneratedTextColumn _constructAddress() {
    return GeneratedTextColumn('address', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _districtMeta = const VerificationMeta('district');
  GeneratedTextColumn _district;
  GeneratedTextColumn get district => _district ??= _constructDistrict();
  GeneratedTextColumn _constructDistrict() {
    return GeneratedTextColumn('district', $tableName, false,
        $customConstraints: 'NOT NULL DEFAULT \'Anantnag\'',
        defaultValue: const CustomExpression<String>('\'Anantnag\''));
  }

  final VerificationMeta _villageCodeMeta =
      const VerificationMeta('villageCode');
  GeneratedTextColumn _villageCode;
  GeneratedTextColumn get villageCode =>
      _villageCode ??= _constructVillageCode();
  GeneratedTextColumn _constructVillageCode() {
    return GeneratedTextColumn('village_code', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _landTypeMeta = const VerificationMeta('landType');
  GeneratedTextColumn _landType;
  GeneratedTextColumn get landType => _landType ??= _constructLandType();
  GeneratedTextColumn _constructLandType() {
    return GeneratedTextColumn('land_type', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _landClassificationMeta =
      const VerificationMeta('landClassification');
  GeneratedTextColumn _landClassification;
  GeneratedTextColumn get landClassification =>
      _landClassification ??= _constructLandClassification();
  GeneratedTextColumn _constructLandClassification() {
    return GeneratedTextColumn('land_classification', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _distanceToNalaMeta =
      const VerificationMeta('distanceToNala');
  GeneratedIntColumn _distanceToNala;
  GeneratedIntColumn get distanceToNala =>
      _distanceToNala ??= _constructDistanceToNala();
  GeneratedIntColumn _constructDistanceToNala() {
    return GeneratedIntColumn('distance_to_nala', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _khasraNumberMeta =
      const VerificationMeta('khasraNumber');
  GeneratedTextColumn _khasraNumber;
  GeneratedTextColumn get khasraNumber =>
      _khasraNumber ??= _constructKhasraNumber();
  GeneratedTextColumn _constructKhasraNumber() {
    return GeneratedTextColumn('khasra_number', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _kanalsMeta = const VerificationMeta('kanals');
  GeneratedIntColumn _kanals;
  GeneratedIntColumn get kanals => _kanals ??= _constructKanals();
  GeneratedIntColumn _constructKanals() {
    return GeneratedIntColumn('kanals', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _marlasMeta = const VerificationMeta('marlas');
  GeneratedRealColumn _marlas;
  GeneratedRealColumn get marlas => _marlas ??= _constructMarlas();
  GeneratedRealColumn _constructMarlas() {
    return GeneratedRealColumn('marlas', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _detailsMeta = const VerificationMeta('details');
  GeneratedTextColumn _details;
  GeneratedTextColumn get details => _details ??= _constructDetails();
  GeneratedTextColumn _constructDetails() {
    return GeneratedTextColumn('details', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _operatorMeta = const VerificationMeta('operator');
  GeneratedTextColumn _operator;
  GeneratedTextColumn get operator => _operator ??= _constructOperator();
  GeneratedTextColumn _constructOperator() {
    return GeneratedTextColumn('operator', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _revOfficial1Meta =
      const VerificationMeta('revOfficial1');
  GeneratedTextColumn _revOfficial1;
  GeneratedTextColumn get revOfficial1 =>
      _revOfficial1 ??= _constructRevOfficial1();
  GeneratedTextColumn _constructRevOfficial1() {
    return GeneratedTextColumn('rev_official1', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _revOfficial2Meta =
      const VerificationMeta('revOfficial2');
  GeneratedTextColumn _revOfficial2;
  GeneratedTextColumn get revOfficial2 =>
      _revOfficial2 ??= _constructRevOfficial2();
  GeneratedTextColumn _constructRevOfficial2() {
    return GeneratedTextColumn('rev_official2', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _forrestOfficialMeta =
      const VerificationMeta('forrestOfficial');
  GeneratedTextColumn _forrestOfficial;
  GeneratedTextColumn get forrestOfficial =>
      _forrestOfficial ??= _constructForrestOfficial();
  GeneratedTextColumn _constructForrestOfficial() {
    return GeneratedTextColumn('forrest_official', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _createdMeta = const VerificationMeta('created');
  GeneratedIntColumn _created;
  GeneratedIntColumn get created => _created ??= _constructCreated();
  GeneratedIntColumn _constructCreated() {
    return GeneratedIntColumn('created', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _modifiedMeta = const VerificationMeta('modified');
  GeneratedIntColumn _modified;
  GeneratedIntColumn get modified => _modified ??= _constructModified();
  GeneratedIntColumn _constructModified() {
    return GeneratedIntColumn('modified', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _fullTextMeta = const VerificationMeta('fullText');
  GeneratedTextColumn _fullText;
  GeneratedTextColumn get fullText => _fullText ??= _constructFullText();
  GeneratedTextColumn _constructFullText() {
    return GeneratedTextColumn('full_text', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _syncedOnMeta = const VerificationMeta('syncedOn');
  GeneratedIntColumn _syncedOn;
  GeneratedIntColumn get syncedOn => _syncedOn ??= _constructSyncedOn();
  GeneratedIntColumn _constructSyncedOn() {
    return GeneratedIntColumn('synced_on', $tableName, false,
        $customConstraints: 'NOT NULL DEFAULT 0',
        defaultValue: const CustomExpression<int>('0'));
  }

  @override
  List<GeneratedColumn> get $columns => [
        id,
        owner,
        parentage,
        mobileNumber,
        location,
        locationManual,
        address,
        district,
        villageCode,
        landType,
        landClassification,
        distanceToNala,
        khasraNumber,
        kanals,
        marlas,
        details,
        operator,
        revOfficial1,
        revOfficial2,
        forrestOfficial,
        created,
        modified,
        fullText,
        syncedOn
      ];
  @override
  Properties get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'properties';
  @override
  final String actualTableName = 'properties';
  @override
  VerificationContext validateIntegrity(Insertable<Property> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('owner')) {
      context.handle(
          _ownerMeta, owner.isAcceptableOrUnknown(data['owner'], _ownerMeta));
    } else if (isInserting) {
      context.missing(_ownerMeta);
    }
    if (data.containsKey('parentage')) {
      context.handle(_parentageMeta,
          parentage.isAcceptableOrUnknown(data['parentage'], _parentageMeta));
    }
    if (data.containsKey('mobile_number')) {
      context.handle(
          _mobileNumberMeta,
          mobileNumber.isAcceptableOrUnknown(
              data['mobile_number'], _mobileNumberMeta));
    }
    if (data.containsKey('location')) {
      context.handle(_locationMeta,
          location.isAcceptableOrUnknown(data['location'], _locationMeta));
    } else if (isInserting) {
      context.missing(_locationMeta);
    }
    if (data.containsKey('location_manual')) {
      context.handle(
          _locationManualMeta,
          locationManual.isAcceptableOrUnknown(
              data['location_manual'], _locationManualMeta));
    }
    if (data.containsKey('address')) {
      context.handle(_addressMeta,
          address.isAcceptableOrUnknown(data['address'], _addressMeta));
    }
    if (data.containsKey('district')) {
      context.handle(_districtMeta,
          district.isAcceptableOrUnknown(data['district'], _districtMeta));
    }
    if (data.containsKey('village_code')) {
      context.handle(
          _villageCodeMeta,
          villageCode.isAcceptableOrUnknown(
              data['village_code'], _villageCodeMeta));
    } else if (isInserting) {
      context.missing(_villageCodeMeta);
    }
    if (data.containsKey('land_type')) {
      context.handle(_landTypeMeta,
          landType.isAcceptableOrUnknown(data['land_type'], _landTypeMeta));
    } else if (isInserting) {
      context.missing(_landTypeMeta);
    }
    if (data.containsKey('land_classification')) {
      context.handle(
          _landClassificationMeta,
          landClassification.isAcceptableOrUnknown(
              data['land_classification'], _landClassificationMeta));
    } else if (isInserting) {
      context.missing(_landClassificationMeta);
    }
    if (data.containsKey('distance_to_nala')) {
      context.handle(
          _distanceToNalaMeta,
          distanceToNala.isAcceptableOrUnknown(
              data['distance_to_nala'], _distanceToNalaMeta));
    }
    if (data.containsKey('khasra_number')) {
      context.handle(
          _khasraNumberMeta,
          khasraNumber.isAcceptableOrUnknown(
              data['khasra_number'], _khasraNumberMeta));
    } else if (isInserting) {
      context.missing(_khasraNumberMeta);
    }
    if (data.containsKey('kanals')) {
      context.handle(_kanalsMeta,
          kanals.isAcceptableOrUnknown(data['kanals'], _kanalsMeta));
    } else if (isInserting) {
      context.missing(_kanalsMeta);
    }
    if (data.containsKey('marlas')) {
      context.handle(_marlasMeta,
          marlas.isAcceptableOrUnknown(data['marlas'], _marlasMeta));
    } else if (isInserting) {
      context.missing(_marlasMeta);
    }
    if (data.containsKey('details')) {
      context.handle(_detailsMeta,
          details.isAcceptableOrUnknown(data['details'], _detailsMeta));
    }
    if (data.containsKey('operator')) {
      context.handle(_operatorMeta,
          operator.isAcceptableOrUnknown(data['operator'], _operatorMeta));
    } else if (isInserting) {
      context.missing(_operatorMeta);
    }
    if (data.containsKey('rev_official1')) {
      context.handle(
          _revOfficial1Meta,
          revOfficial1.isAcceptableOrUnknown(
              data['rev_official1'], _revOfficial1Meta));
    } else if (isInserting) {
      context.missing(_revOfficial1Meta);
    }
    if (data.containsKey('rev_official2')) {
      context.handle(
          _revOfficial2Meta,
          revOfficial2.isAcceptableOrUnknown(
              data['rev_official2'], _revOfficial2Meta));
    } else if (isInserting) {
      context.missing(_revOfficial2Meta);
    }
    if (data.containsKey('forrest_official')) {
      context.handle(
          _forrestOfficialMeta,
          forrestOfficial.isAcceptableOrUnknown(
              data['forrest_official'], _forrestOfficialMeta));
    } else if (isInserting) {
      context.missing(_forrestOfficialMeta);
    }
    if (data.containsKey('created')) {
      context.handle(_createdMeta,
          created.isAcceptableOrUnknown(data['created'], _createdMeta));
    } else if (isInserting) {
      context.missing(_createdMeta);
    }
    if (data.containsKey('modified')) {
      context.handle(_modifiedMeta,
          modified.isAcceptableOrUnknown(data['modified'], _modifiedMeta));
    } else if (isInserting) {
      context.missing(_modifiedMeta);
    }
    if (data.containsKey('full_text')) {
      context.handle(_fullTextMeta,
          fullText.isAcceptableOrUnknown(data['full_text'], _fullTextMeta));
    } else if (isInserting) {
      context.missing(_fullTextMeta);
    }
    if (data.containsKey('synced_on')) {
      context.handle(_syncedOnMeta,
          syncedOn.isAcceptableOrUnknown(data['synced_on'], _syncedOnMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Property map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Property.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Properties createAlias(String alias) {
    return Properties(_db, alias);
  }

  @override
  List<String> get customConstraints => const [
        'FOREIGN KEY("village_code") REFERENCES "villages"("code") ON DELETE SET NULL'
      ];
  @override
  bool get dontWriteConstraints => true;
}

class Structure extends DataClass implements Insertable<Structure> {
  final int id;
  final int propertyId;
  final String photos;
  final String location;
  final String locationManual;
  final String buildType;
  final String classification;
  final String permissionType;
  final int roomCount;
  final int constructionYear;
  final int area;
  final int distanceToNala;
  final String details;
  final String operator;
  final String revOfficial1;
  final String revOfficial2;
  final String forrestOfficial;
  final int created;
  final int modified;
  Structure(
      {@required this.id,
      @required this.propertyId,
      this.photos,
      this.location,
      this.locationManual,
      this.buildType,
      this.classification,
      this.permissionType,
      this.roomCount,
      this.constructionYear,
      this.area,
      this.distanceToNala,
      this.details,
      this.operator,
      this.revOfficial1,
      this.revOfficial2,
      this.forrestOfficial,
      this.created,
      this.modified});
  factory Structure.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return Structure(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      propertyId: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}property_id']),
      photos:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}photos']),
      location: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}location']),
      locationManual: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}location_manual']),
      buildType: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}build_type']),
      classification: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}classification']),
      permissionType: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}permission_type']),
      roomCount:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}room_count']),
      constructionYear: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}construction_year']),
      area: intType.mapFromDatabaseResponse(data['${effectivePrefix}area']),
      distanceToNala: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}distance_to_nala']),
      details:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}details']),
      operator: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}operator']),
      revOfficial1: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}rev_official1']),
      revOfficial2: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}rev_official2']),
      forrestOfficial: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}forrest_official']),
      created:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}created']),
      modified:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}modified']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || propertyId != null) {
      map['property_id'] = Variable<int>(propertyId);
    }
    if (!nullToAbsent || photos != null) {
      map['photos'] = Variable<String>(photos);
    }
    if (!nullToAbsent || location != null) {
      map['location'] = Variable<String>(location);
    }
    if (!nullToAbsent || locationManual != null) {
      map['location_manual'] = Variable<String>(locationManual);
    }
    if (!nullToAbsent || buildType != null) {
      map['build_type'] = Variable<String>(buildType);
    }
    if (!nullToAbsent || classification != null) {
      map['classification'] = Variable<String>(classification);
    }
    if (!nullToAbsent || permissionType != null) {
      map['permission_type'] = Variable<String>(permissionType);
    }
    if (!nullToAbsent || roomCount != null) {
      map['room_count'] = Variable<int>(roomCount);
    }
    if (!nullToAbsent || constructionYear != null) {
      map['construction_year'] = Variable<int>(constructionYear);
    }
    if (!nullToAbsent || area != null) {
      map['area'] = Variable<int>(area);
    }
    if (!nullToAbsent || distanceToNala != null) {
      map['distance_to_nala'] = Variable<int>(distanceToNala);
    }
    if (!nullToAbsent || details != null) {
      map['details'] = Variable<String>(details);
    }
    if (!nullToAbsent || operator != null) {
      map['operator'] = Variable<String>(operator);
    }
    if (!nullToAbsent || revOfficial1 != null) {
      map['rev_official1'] = Variable<String>(revOfficial1);
    }
    if (!nullToAbsent || revOfficial2 != null) {
      map['rev_official2'] = Variable<String>(revOfficial2);
    }
    if (!nullToAbsent || forrestOfficial != null) {
      map['forrest_official'] = Variable<String>(forrestOfficial);
    }
    if (!nullToAbsent || created != null) {
      map['created'] = Variable<int>(created);
    }
    if (!nullToAbsent || modified != null) {
      map['modified'] = Variable<int>(modified);
    }
    return map;
  }

  StructuresCompanion toCompanion(bool nullToAbsent) {
    return StructuresCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      propertyId: propertyId == null && nullToAbsent
          ? const Value.absent()
          : Value(propertyId),
      photos:
          photos == null && nullToAbsent ? const Value.absent() : Value(photos),
      location: location == null && nullToAbsent
          ? const Value.absent()
          : Value(location),
      locationManual: locationManual == null && nullToAbsent
          ? const Value.absent()
          : Value(locationManual),
      buildType: buildType == null && nullToAbsent
          ? const Value.absent()
          : Value(buildType),
      classification: classification == null && nullToAbsent
          ? const Value.absent()
          : Value(classification),
      permissionType: permissionType == null && nullToAbsent
          ? const Value.absent()
          : Value(permissionType),
      roomCount: roomCount == null && nullToAbsent
          ? const Value.absent()
          : Value(roomCount),
      constructionYear: constructionYear == null && nullToAbsent
          ? const Value.absent()
          : Value(constructionYear),
      area: area == null && nullToAbsent ? const Value.absent() : Value(area),
      distanceToNala: distanceToNala == null && nullToAbsent
          ? const Value.absent()
          : Value(distanceToNala),
      details: details == null && nullToAbsent
          ? const Value.absent()
          : Value(details),
      operator: operator == null && nullToAbsent
          ? const Value.absent()
          : Value(operator),
      revOfficial1: revOfficial1 == null && nullToAbsent
          ? const Value.absent()
          : Value(revOfficial1),
      revOfficial2: revOfficial2 == null && nullToAbsent
          ? const Value.absent()
          : Value(revOfficial2),
      forrestOfficial: forrestOfficial == null && nullToAbsent
          ? const Value.absent()
          : Value(forrestOfficial),
      created: created == null && nullToAbsent
          ? const Value.absent()
          : Value(created),
      modified: modified == null && nullToAbsent
          ? const Value.absent()
          : Value(modified),
    );
  }

  factory Structure.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Structure(
      id: serializer.fromJson<int>(json['id']),
      propertyId: serializer.fromJson<int>(json['property_id']),
      photos: serializer.fromJson<String>(json['photos']),
      location: serializer.fromJson<String>(json['location']),
      locationManual: serializer.fromJson<String>(json['location_manual']),
      buildType: serializer.fromJson<String>(json['build_type']),
      classification: serializer.fromJson<String>(json['classification']),
      permissionType: serializer.fromJson<String>(json['permission_type']),
      roomCount: serializer.fromJson<int>(json['room_count']),
      constructionYear: serializer.fromJson<int>(json['construction_year']),
      area: serializer.fromJson<int>(json['area']),
      distanceToNala: serializer.fromJson<int>(json['distance_to_nala']),
      details: serializer.fromJson<String>(json['details']),
      operator: serializer.fromJson<String>(json['operator']),
      revOfficial1: serializer.fromJson<String>(json['rev_official1']),
      revOfficial2: serializer.fromJson<String>(json['rev_official2']),
      forrestOfficial: serializer.fromJson<String>(json['forrest_official']),
      created: serializer.fromJson<int>(json['created']),
      modified: serializer.fromJson<int>(json['modified']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'property_id': serializer.toJson<int>(propertyId),
      'photos': serializer.toJson<String>(photos),
      'location': serializer.toJson<String>(location),
      'location_manual': serializer.toJson<String>(locationManual),
      'build_type': serializer.toJson<String>(buildType),
      'classification': serializer.toJson<String>(classification),
      'permission_type': serializer.toJson<String>(permissionType),
      'room_count': serializer.toJson<int>(roomCount),
      'construction_year': serializer.toJson<int>(constructionYear),
      'area': serializer.toJson<int>(area),
      'distance_to_nala': serializer.toJson<int>(distanceToNala),
      'details': serializer.toJson<String>(details),
      'operator': serializer.toJson<String>(operator),
      'rev_official1': serializer.toJson<String>(revOfficial1),
      'rev_official2': serializer.toJson<String>(revOfficial2),
      'forrest_official': serializer.toJson<String>(forrestOfficial),
      'created': serializer.toJson<int>(created),
      'modified': serializer.toJson<int>(modified),
    };
  }

  Structure copyWith(
          {int id,
          int propertyId,
          String photos,
          String location,
          String locationManual,
          String buildType,
          String classification,
          String permissionType,
          int roomCount,
          int constructionYear,
          int area,
          int distanceToNala,
          String details,
          String operator,
          String revOfficial1,
          String revOfficial2,
          String forrestOfficial,
          int created,
          int modified}) =>
      Structure(
        id: id ?? this.id,
        propertyId: propertyId ?? this.propertyId,
        photos: photos ?? this.photos,
        location: location ?? this.location,
        locationManual: locationManual ?? this.locationManual,
        buildType: buildType ?? this.buildType,
        classification: classification ?? this.classification,
        permissionType: permissionType ?? this.permissionType,
        roomCount: roomCount ?? this.roomCount,
        constructionYear: constructionYear ?? this.constructionYear,
        area: area ?? this.area,
        distanceToNala: distanceToNala ?? this.distanceToNala,
        details: details ?? this.details,
        operator: operator ?? this.operator,
        revOfficial1: revOfficial1 ?? this.revOfficial1,
        revOfficial2: revOfficial2 ?? this.revOfficial2,
        forrestOfficial: forrestOfficial ?? this.forrestOfficial,
        created: created ?? this.created,
        modified: modified ?? this.modified,
      );
  @override
  String toString() {
    return (StringBuffer('Structure(')
          ..write('id: $id, ')
          ..write('propertyId: $propertyId, ')
          ..write('photos: $photos, ')
          ..write('location: $location, ')
          ..write('locationManual: $locationManual, ')
          ..write('buildType: $buildType, ')
          ..write('classification: $classification, ')
          ..write('permissionType: $permissionType, ')
          ..write('roomCount: $roomCount, ')
          ..write('constructionYear: $constructionYear, ')
          ..write('area: $area, ')
          ..write('distanceToNala: $distanceToNala, ')
          ..write('details: $details, ')
          ..write('operator: $operator, ')
          ..write('revOfficial1: $revOfficial1, ')
          ..write('revOfficial2: $revOfficial2, ')
          ..write('forrestOfficial: $forrestOfficial, ')
          ..write('created: $created, ')
          ..write('modified: $modified')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          propertyId.hashCode,
          $mrjc(
              photos.hashCode,
              $mrjc(
                  location.hashCode,
                  $mrjc(
                      locationManual.hashCode,
                      $mrjc(
                          buildType.hashCode,
                          $mrjc(
                              classification.hashCode,
                              $mrjc(
                                  permissionType.hashCode,
                                  $mrjc(
                                      roomCount.hashCode,
                                      $mrjc(
                                          constructionYear.hashCode,
                                          $mrjc(
                                              area.hashCode,
                                              $mrjc(
                                                  distanceToNala.hashCode,
                                                  $mrjc(
                                                      details.hashCode,
                                                      $mrjc(
                                                          operator.hashCode,
                                                          $mrjc(
                                                              revOfficial1
                                                                  .hashCode,
                                                              $mrjc(
                                                                  revOfficial2
                                                                      .hashCode,
                                                                  $mrjc(
                                                                      forrestOfficial
                                                                          .hashCode,
                                                                      $mrjc(
                                                                          created
                                                                              .hashCode,
                                                                          modified
                                                                              .hashCode)))))))))))))))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Structure &&
          other.id == this.id &&
          other.propertyId == this.propertyId &&
          other.photos == this.photos &&
          other.location == this.location &&
          other.locationManual == this.locationManual &&
          other.buildType == this.buildType &&
          other.classification == this.classification &&
          other.permissionType == this.permissionType &&
          other.roomCount == this.roomCount &&
          other.constructionYear == this.constructionYear &&
          other.area == this.area &&
          other.distanceToNala == this.distanceToNala &&
          other.details == this.details &&
          other.operator == this.operator &&
          other.revOfficial1 == this.revOfficial1 &&
          other.revOfficial2 == this.revOfficial2 &&
          other.forrestOfficial == this.forrestOfficial &&
          other.created == this.created &&
          other.modified == this.modified);
}

class StructuresCompanion extends UpdateCompanion<Structure> {
  final Value<int> id;
  final Value<int> propertyId;
  final Value<String> photos;
  final Value<String> location;
  final Value<String> locationManual;
  final Value<String> buildType;
  final Value<String> classification;
  final Value<String> permissionType;
  final Value<int> roomCount;
  final Value<int> constructionYear;
  final Value<int> area;
  final Value<int> distanceToNala;
  final Value<String> details;
  final Value<String> operator;
  final Value<String> revOfficial1;
  final Value<String> revOfficial2;
  final Value<String> forrestOfficial;
  final Value<int> created;
  final Value<int> modified;
  const StructuresCompanion({
    this.id = const Value.absent(),
    this.propertyId = const Value.absent(),
    this.photos = const Value.absent(),
    this.location = const Value.absent(),
    this.locationManual = const Value.absent(),
    this.buildType = const Value.absent(),
    this.classification = const Value.absent(),
    this.permissionType = const Value.absent(),
    this.roomCount = const Value.absent(),
    this.constructionYear = const Value.absent(),
    this.area = const Value.absent(),
    this.distanceToNala = const Value.absent(),
    this.details = const Value.absent(),
    this.operator = const Value.absent(),
    this.revOfficial1 = const Value.absent(),
    this.revOfficial2 = const Value.absent(),
    this.forrestOfficial = const Value.absent(),
    this.created = const Value.absent(),
    this.modified = const Value.absent(),
  });
  StructuresCompanion.insert({
    this.id = const Value.absent(),
    @required int propertyId,
    this.photos = const Value.absent(),
    this.location = const Value.absent(),
    this.locationManual = const Value.absent(),
    this.buildType = const Value.absent(),
    this.classification = const Value.absent(),
    this.permissionType = const Value.absent(),
    this.roomCount = const Value.absent(),
    this.constructionYear = const Value.absent(),
    this.area = const Value.absent(),
    this.distanceToNala = const Value.absent(),
    this.details = const Value.absent(),
    this.operator = const Value.absent(),
    this.revOfficial1 = const Value.absent(),
    this.revOfficial2 = const Value.absent(),
    this.forrestOfficial = const Value.absent(),
    this.created = const Value.absent(),
    this.modified = const Value.absent(),
  }) : propertyId = Value(propertyId);
  static Insertable<Structure> custom({
    Expression<int> id,
    Expression<int> propertyId,
    Expression<String> photos,
    Expression<String> location,
    Expression<String> locationManual,
    Expression<String> buildType,
    Expression<String> classification,
    Expression<String> permissionType,
    Expression<int> roomCount,
    Expression<int> constructionYear,
    Expression<int> area,
    Expression<int> distanceToNala,
    Expression<String> details,
    Expression<String> operator,
    Expression<String> revOfficial1,
    Expression<String> revOfficial2,
    Expression<String> forrestOfficial,
    Expression<int> created,
    Expression<int> modified,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (propertyId != null) 'property_id': propertyId,
      if (photos != null) 'photos': photos,
      if (location != null) 'location': location,
      if (locationManual != null) 'location_manual': locationManual,
      if (buildType != null) 'build_type': buildType,
      if (classification != null) 'classification': classification,
      if (permissionType != null) 'permission_type': permissionType,
      if (roomCount != null) 'room_count': roomCount,
      if (constructionYear != null) 'construction_year': constructionYear,
      if (area != null) 'area': area,
      if (distanceToNala != null) 'distance_to_nala': distanceToNala,
      if (details != null) 'details': details,
      if (operator != null) 'operator': operator,
      if (revOfficial1 != null) 'rev_official1': revOfficial1,
      if (revOfficial2 != null) 'rev_official2': revOfficial2,
      if (forrestOfficial != null) 'forrest_official': forrestOfficial,
      if (created != null) 'created': created,
      if (modified != null) 'modified': modified,
    });
  }

  StructuresCompanion copyWith(
      {Value<int> id,
      Value<int> propertyId,
      Value<String> photos,
      Value<String> location,
      Value<String> locationManual,
      Value<String> buildType,
      Value<String> classification,
      Value<String> permissionType,
      Value<int> roomCount,
      Value<int> constructionYear,
      Value<int> area,
      Value<int> distanceToNala,
      Value<String> details,
      Value<String> operator,
      Value<String> revOfficial1,
      Value<String> revOfficial2,
      Value<String> forrestOfficial,
      Value<int> created,
      Value<int> modified}) {
    return StructuresCompanion(
      id: id ?? this.id,
      propertyId: propertyId ?? this.propertyId,
      photos: photos ?? this.photos,
      location: location ?? this.location,
      locationManual: locationManual ?? this.locationManual,
      buildType: buildType ?? this.buildType,
      classification: classification ?? this.classification,
      permissionType: permissionType ?? this.permissionType,
      roomCount: roomCount ?? this.roomCount,
      constructionYear: constructionYear ?? this.constructionYear,
      area: area ?? this.area,
      distanceToNala: distanceToNala ?? this.distanceToNala,
      details: details ?? this.details,
      operator: operator ?? this.operator,
      revOfficial1: revOfficial1 ?? this.revOfficial1,
      revOfficial2: revOfficial2 ?? this.revOfficial2,
      forrestOfficial: forrestOfficial ?? this.forrestOfficial,
      created: created ?? this.created,
      modified: modified ?? this.modified,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (propertyId.present) {
      map['property_id'] = Variable<int>(propertyId.value);
    }
    if (photos.present) {
      map['photos'] = Variable<String>(photos.value);
    }
    if (location.present) {
      map['location'] = Variable<String>(location.value);
    }
    if (locationManual.present) {
      map['location_manual'] = Variable<String>(locationManual.value);
    }
    if (buildType.present) {
      map['build_type'] = Variable<String>(buildType.value);
    }
    if (classification.present) {
      map['classification'] = Variable<String>(classification.value);
    }
    if (permissionType.present) {
      map['permission_type'] = Variable<String>(permissionType.value);
    }
    if (roomCount.present) {
      map['room_count'] = Variable<int>(roomCount.value);
    }
    if (constructionYear.present) {
      map['construction_year'] = Variable<int>(constructionYear.value);
    }
    if (area.present) {
      map['area'] = Variable<int>(area.value);
    }
    if (distanceToNala.present) {
      map['distance_to_nala'] = Variable<int>(distanceToNala.value);
    }
    if (details.present) {
      map['details'] = Variable<String>(details.value);
    }
    if (operator.present) {
      map['operator'] = Variable<String>(operator.value);
    }
    if (revOfficial1.present) {
      map['rev_official1'] = Variable<String>(revOfficial1.value);
    }
    if (revOfficial2.present) {
      map['rev_official2'] = Variable<String>(revOfficial2.value);
    }
    if (forrestOfficial.present) {
      map['forrest_official'] = Variable<String>(forrestOfficial.value);
    }
    if (created.present) {
      map['created'] = Variable<int>(created.value);
    }
    if (modified.present) {
      map['modified'] = Variable<int>(modified.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('StructuresCompanion(')
          ..write('id: $id, ')
          ..write('propertyId: $propertyId, ')
          ..write('photos: $photos, ')
          ..write('location: $location, ')
          ..write('locationManual: $locationManual, ')
          ..write('buildType: $buildType, ')
          ..write('classification: $classification, ')
          ..write('permissionType: $permissionType, ')
          ..write('roomCount: $roomCount, ')
          ..write('constructionYear: $constructionYear, ')
          ..write('area: $area, ')
          ..write('distanceToNala: $distanceToNala, ')
          ..write('details: $details, ')
          ..write('operator: $operator, ')
          ..write('revOfficial1: $revOfficial1, ')
          ..write('revOfficial2: $revOfficial2, ')
          ..write('forrestOfficial: $forrestOfficial, ')
          ..write('created: $created, ')
          ..write('modified: $modified')
          ..write(')'))
        .toString();
  }
}

class Structures extends Table with TableInfo<Structures, Structure> {
  final GeneratedDatabase _db;
  final String _alias;
  Structures(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        declaredAsPrimaryKey: true,
        hasAutoIncrement: true,
        $customConstraints: 'NOT NULL PRIMARY KEY AUTOINCREMENT');
  }

  final VerificationMeta _propertyIdMeta = const VerificationMeta('propertyId');
  GeneratedIntColumn _propertyId;
  GeneratedIntColumn get propertyId => _propertyId ??= _constructPropertyId();
  GeneratedIntColumn _constructPropertyId() {
    return GeneratedIntColumn('property_id', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _photosMeta = const VerificationMeta('photos');
  GeneratedTextColumn _photos;
  GeneratedTextColumn get photos => _photos ??= _constructPhotos();
  GeneratedTextColumn _constructPhotos() {
    return GeneratedTextColumn('photos', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _locationMeta = const VerificationMeta('location');
  GeneratedTextColumn _location;
  GeneratedTextColumn get location => _location ??= _constructLocation();
  GeneratedTextColumn _constructLocation() {
    return GeneratedTextColumn('location', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _locationManualMeta =
      const VerificationMeta('locationManual');
  GeneratedTextColumn _locationManual;
  GeneratedTextColumn get locationManual =>
      _locationManual ??= _constructLocationManual();
  GeneratedTextColumn _constructLocationManual() {
    return GeneratedTextColumn('location_manual', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _buildTypeMeta = const VerificationMeta('buildType');
  GeneratedTextColumn _buildType;
  GeneratedTextColumn get buildType => _buildType ??= _constructBuildType();
  GeneratedTextColumn _constructBuildType() {
    return GeneratedTextColumn('build_type', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _classificationMeta =
      const VerificationMeta('classification');
  GeneratedTextColumn _classification;
  GeneratedTextColumn get classification =>
      _classification ??= _constructClassification();
  GeneratedTextColumn _constructClassification() {
    return GeneratedTextColumn('classification', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _permissionTypeMeta =
      const VerificationMeta('permissionType');
  GeneratedTextColumn _permissionType;
  GeneratedTextColumn get permissionType =>
      _permissionType ??= _constructPermissionType();
  GeneratedTextColumn _constructPermissionType() {
    return GeneratedTextColumn('permission_type', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _roomCountMeta = const VerificationMeta('roomCount');
  GeneratedIntColumn _roomCount;
  GeneratedIntColumn get roomCount => _roomCount ??= _constructRoomCount();
  GeneratedIntColumn _constructRoomCount() {
    return GeneratedIntColumn('room_count', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _constructionYearMeta =
      const VerificationMeta('constructionYear');
  GeneratedIntColumn _constructionYear;
  GeneratedIntColumn get constructionYear =>
      _constructionYear ??= _constructConstructionYear();
  GeneratedIntColumn _constructConstructionYear() {
    return GeneratedIntColumn('construction_year', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _areaMeta = const VerificationMeta('area');
  GeneratedIntColumn _area;
  GeneratedIntColumn get area => _area ??= _constructArea();
  GeneratedIntColumn _constructArea() {
    return GeneratedIntColumn('area', $tableName, true, $customConstraints: '');
  }

  final VerificationMeta _distanceToNalaMeta =
      const VerificationMeta('distanceToNala');
  GeneratedIntColumn _distanceToNala;
  GeneratedIntColumn get distanceToNala =>
      _distanceToNala ??= _constructDistanceToNala();
  GeneratedIntColumn _constructDistanceToNala() {
    return GeneratedIntColumn('distance_to_nala', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _detailsMeta = const VerificationMeta('details');
  GeneratedTextColumn _details;
  GeneratedTextColumn get details => _details ??= _constructDetails();
  GeneratedTextColumn _constructDetails() {
    return GeneratedTextColumn('details', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _operatorMeta = const VerificationMeta('operator');
  GeneratedTextColumn _operator;
  GeneratedTextColumn get operator => _operator ??= _constructOperator();
  GeneratedTextColumn _constructOperator() {
    return GeneratedTextColumn('operator', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _revOfficial1Meta =
      const VerificationMeta('revOfficial1');
  GeneratedTextColumn _revOfficial1;
  GeneratedTextColumn get revOfficial1 =>
      _revOfficial1 ??= _constructRevOfficial1();
  GeneratedTextColumn _constructRevOfficial1() {
    return GeneratedTextColumn('rev_official1', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _revOfficial2Meta =
      const VerificationMeta('revOfficial2');
  GeneratedTextColumn _revOfficial2;
  GeneratedTextColumn get revOfficial2 =>
      _revOfficial2 ??= _constructRevOfficial2();
  GeneratedTextColumn _constructRevOfficial2() {
    return GeneratedTextColumn('rev_official2', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _forrestOfficialMeta =
      const VerificationMeta('forrestOfficial');
  GeneratedTextColumn _forrestOfficial;
  GeneratedTextColumn get forrestOfficial =>
      _forrestOfficial ??= _constructForrestOfficial();
  GeneratedTextColumn _constructForrestOfficial() {
    return GeneratedTextColumn('forrest_official', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _createdMeta = const VerificationMeta('created');
  GeneratedIntColumn _created;
  GeneratedIntColumn get created => _created ??= _constructCreated();
  GeneratedIntColumn _constructCreated() {
    return GeneratedIntColumn('created', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _modifiedMeta = const VerificationMeta('modified');
  GeneratedIntColumn _modified;
  GeneratedIntColumn get modified => _modified ??= _constructModified();
  GeneratedIntColumn _constructModified() {
    return GeneratedIntColumn('modified', $tableName, true,
        $customConstraints: '');
  }

  @override
  List<GeneratedColumn> get $columns => [
        id,
        propertyId,
        photos,
        location,
        locationManual,
        buildType,
        classification,
        permissionType,
        roomCount,
        constructionYear,
        area,
        distanceToNala,
        details,
        operator,
        revOfficial1,
        revOfficial2,
        forrestOfficial,
        created,
        modified
      ];
  @override
  Structures get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'structures';
  @override
  final String actualTableName = 'structures';
  @override
  VerificationContext validateIntegrity(Insertable<Structure> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('property_id')) {
      context.handle(
          _propertyIdMeta,
          propertyId.isAcceptableOrUnknown(
              data['property_id'], _propertyIdMeta));
    } else if (isInserting) {
      context.missing(_propertyIdMeta);
    }
    if (data.containsKey('photos')) {
      context.handle(_photosMeta,
          photos.isAcceptableOrUnknown(data['photos'], _photosMeta));
    }
    if (data.containsKey('location')) {
      context.handle(_locationMeta,
          location.isAcceptableOrUnknown(data['location'], _locationMeta));
    }
    if (data.containsKey('location_manual')) {
      context.handle(
          _locationManualMeta,
          locationManual.isAcceptableOrUnknown(
              data['location_manual'], _locationManualMeta));
    }
    if (data.containsKey('build_type')) {
      context.handle(_buildTypeMeta,
          buildType.isAcceptableOrUnknown(data['build_type'], _buildTypeMeta));
    }
    if (data.containsKey('classification')) {
      context.handle(
          _classificationMeta,
          classification.isAcceptableOrUnknown(
              data['classification'], _classificationMeta));
    }
    if (data.containsKey('permission_type')) {
      context.handle(
          _permissionTypeMeta,
          permissionType.isAcceptableOrUnknown(
              data['permission_type'], _permissionTypeMeta));
    }
    if (data.containsKey('room_count')) {
      context.handle(_roomCountMeta,
          roomCount.isAcceptableOrUnknown(data['room_count'], _roomCountMeta));
    }
    if (data.containsKey('construction_year')) {
      context.handle(
          _constructionYearMeta,
          constructionYear.isAcceptableOrUnknown(
              data['construction_year'], _constructionYearMeta));
    }
    if (data.containsKey('area')) {
      context.handle(
          _areaMeta, area.isAcceptableOrUnknown(data['area'], _areaMeta));
    }
    if (data.containsKey('distance_to_nala')) {
      context.handle(
          _distanceToNalaMeta,
          distanceToNala.isAcceptableOrUnknown(
              data['distance_to_nala'], _distanceToNalaMeta));
    }
    if (data.containsKey('details')) {
      context.handle(_detailsMeta,
          details.isAcceptableOrUnknown(data['details'], _detailsMeta));
    }
    if (data.containsKey('operator')) {
      context.handle(_operatorMeta,
          operator.isAcceptableOrUnknown(data['operator'], _operatorMeta));
    }
    if (data.containsKey('rev_official1')) {
      context.handle(
          _revOfficial1Meta,
          revOfficial1.isAcceptableOrUnknown(
              data['rev_official1'], _revOfficial1Meta));
    }
    if (data.containsKey('rev_official2')) {
      context.handle(
          _revOfficial2Meta,
          revOfficial2.isAcceptableOrUnknown(
              data['rev_official2'], _revOfficial2Meta));
    }
    if (data.containsKey('forrest_official')) {
      context.handle(
          _forrestOfficialMeta,
          forrestOfficial.isAcceptableOrUnknown(
              data['forrest_official'], _forrestOfficialMeta));
    }
    if (data.containsKey('created')) {
      context.handle(_createdMeta,
          created.isAcceptableOrUnknown(data['created'], _createdMeta));
    }
    if (data.containsKey('modified')) {
      context.handle(_modifiedMeta,
          modified.isAcceptableOrUnknown(data['modified'], _modifiedMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Structure map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Structure.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Structures createAlias(String alias) {
    return Structures(_db, alias);
  }

  @override
  List<String> get customConstraints => const [
        'FOREIGN KEY("property_id") REFERENCES "properties"("id") ON DELETE CASCADE'
      ];
  @override
  bool get dontWriteConstraints => true;
}

class StructurePermissionType extends DataClass
    implements Insertable<StructurePermissionType> {
  final String name;
  StructurePermissionType({@required this.name});
  factory StructurePermissionType.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    return StructurePermissionType(
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String>(name);
    }
    return map;
  }

  StructurePermissionTypesCompanion toCompanion(bool nullToAbsent) {
    return StructurePermissionTypesCompanion(
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
    );
  }

  factory StructurePermissionType.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return StructurePermissionType(
      name: serializer.fromJson<String>(json['name']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'name': serializer.toJson<String>(name),
    };
  }

  StructurePermissionType copyWith({String name}) => StructurePermissionType(
        name: name ?? this.name,
      );
  @override
  String toString() {
    return (StringBuffer('StructurePermissionType(')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf(name.hashCode);
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is StructurePermissionType && other.name == this.name);
}

class StructurePermissionTypesCompanion
    extends UpdateCompanion<StructurePermissionType> {
  final Value<String> name;
  const StructurePermissionTypesCompanion({
    this.name = const Value.absent(),
  });
  StructurePermissionTypesCompanion.insert({
    @required String name,
  }) : name = Value(name);
  static Insertable<StructurePermissionType> custom({
    Expression<String> name,
  }) {
    return RawValuesInsertable({
      if (name != null) 'name': name,
    });
  }

  StructurePermissionTypesCompanion copyWith({Value<String> name}) {
    return StructurePermissionTypesCompanion(
      name: name ?? this.name,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('StructurePermissionTypesCompanion(')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }
}

class StructurePermissionTypes extends Table
    with TableInfo<StructurePermissionTypes, StructurePermissionType> {
  final GeneratedDatabase _db;
  final String _alias;
  StructurePermissionTypes(this._db, [this._alias]);
  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn('name', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  @override
  List<GeneratedColumn> get $columns => [name];
  @override
  StructurePermissionTypes get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'structure_permission_types';
  @override
  final String actualTableName = 'structure_permission_types';
  @override
  VerificationContext validateIntegrity(
      Insertable<StructurePermissionType> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name'], _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  StructurePermissionType map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return StructurePermissionType.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  StructurePermissionTypes createAlias(String alias) {
    return StructurePermissionTypes(_db, alias);
  }

  @override
  bool get dontWriteConstraints => true;
}

class StructureClassification extends DataClass
    implements Insertable<StructureClassification> {
  final String name;
  StructureClassification({@required this.name});
  factory StructureClassification.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    return StructureClassification(
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String>(name);
    }
    return map;
  }

  StructureClassificationsCompanion toCompanion(bool nullToAbsent) {
    return StructureClassificationsCompanion(
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
    );
  }

  factory StructureClassification.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return StructureClassification(
      name: serializer.fromJson<String>(json['name']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'name': serializer.toJson<String>(name),
    };
  }

  StructureClassification copyWith({String name}) => StructureClassification(
        name: name ?? this.name,
      );
  @override
  String toString() {
    return (StringBuffer('StructureClassification(')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf(name.hashCode);
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is StructureClassification && other.name == this.name);
}

class StructureClassificationsCompanion
    extends UpdateCompanion<StructureClassification> {
  final Value<String> name;
  const StructureClassificationsCompanion({
    this.name = const Value.absent(),
  });
  StructureClassificationsCompanion.insert({
    @required String name,
  }) : name = Value(name);
  static Insertable<StructureClassification> custom({
    Expression<String> name,
  }) {
    return RawValuesInsertable({
      if (name != null) 'name': name,
    });
  }

  StructureClassificationsCompanion copyWith({Value<String> name}) {
    return StructureClassificationsCompanion(
      name: name ?? this.name,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('StructureClassificationsCompanion(')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }
}

class StructureClassifications extends Table
    with TableInfo<StructureClassifications, StructureClassification> {
  final GeneratedDatabase _db;
  final String _alias;
  StructureClassifications(this._db, [this._alias]);
  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn('name', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  @override
  List<GeneratedColumn> get $columns => [name];
  @override
  StructureClassifications get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'structure_classifications';
  @override
  final String actualTableName = 'structure_classifications';
  @override
  VerificationContext validateIntegrity(
      Insertable<StructureClassification> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name'], _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  StructureClassification map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return StructureClassification.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  StructureClassifications createAlias(String alias) {
    return StructureClassifications(_db, alias);
  }

  @override
  bool get dontWriteConstraints => true;
}

class StructureBuildType extends DataClass
    implements Insertable<StructureBuildType> {
  final String name;
  StructureBuildType({@required this.name});
  factory StructureBuildType.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    return StructureBuildType(
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String>(name);
    }
    return map;
  }

  StructureBuildTypesCompanion toCompanion(bool nullToAbsent) {
    return StructureBuildTypesCompanion(
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
    );
  }

  factory StructureBuildType.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return StructureBuildType(
      name: serializer.fromJson<String>(json['name']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'name': serializer.toJson<String>(name),
    };
  }

  StructureBuildType copyWith({String name}) => StructureBuildType(
        name: name ?? this.name,
      );
  @override
  String toString() {
    return (StringBuffer('StructureBuildType(')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf(name.hashCode);
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is StructureBuildType && other.name == this.name);
}

class StructureBuildTypesCompanion extends UpdateCompanion<StructureBuildType> {
  final Value<String> name;
  const StructureBuildTypesCompanion({
    this.name = const Value.absent(),
  });
  StructureBuildTypesCompanion.insert({
    @required String name,
  }) : name = Value(name);
  static Insertable<StructureBuildType> custom({
    Expression<String> name,
  }) {
    return RawValuesInsertable({
      if (name != null) 'name': name,
    });
  }

  StructureBuildTypesCompanion copyWith({Value<String> name}) {
    return StructureBuildTypesCompanion(
      name: name ?? this.name,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('StructureBuildTypesCompanion(')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }
}

class StructureBuildTypes extends Table
    with TableInfo<StructureBuildTypes, StructureBuildType> {
  final GeneratedDatabase _db;
  final String _alias;
  StructureBuildTypes(this._db, [this._alias]);
  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn('name', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  @override
  List<GeneratedColumn> get $columns => [name];
  @override
  StructureBuildTypes get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'structure_build_types';
  @override
  final String actualTableName = 'structure_build_types';
  @override
  VerificationContext validateIntegrity(Insertable<StructureBuildType> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name'], _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  StructureBuildType map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return StructureBuildType.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  StructureBuildTypes createAlias(String alias) {
    return StructureBuildTypes(_db, alias);
  }

  @override
  bool get dontWriteConstraints => true;
}

class LandType extends DataClass implements Insertable<LandType> {
  final String name;
  LandType({@required this.name});
  factory LandType.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    return LandType(
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String>(name);
    }
    return map;
  }

  LandTypesCompanion toCompanion(bool nullToAbsent) {
    return LandTypesCompanion(
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
    );
  }

  factory LandType.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return LandType(
      name: serializer.fromJson<String>(json['name']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'name': serializer.toJson<String>(name),
    };
  }

  LandType copyWith({String name}) => LandType(
        name: name ?? this.name,
      );
  @override
  String toString() {
    return (StringBuffer('LandType(')..write('name: $name')..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf(name.hashCode);
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) || (other is LandType && other.name == this.name);
}

class LandTypesCompanion extends UpdateCompanion<LandType> {
  final Value<String> name;
  const LandTypesCompanion({
    this.name = const Value.absent(),
  });
  LandTypesCompanion.insert({
    @required String name,
  }) : name = Value(name);
  static Insertable<LandType> custom({
    Expression<String> name,
  }) {
    return RawValuesInsertable({
      if (name != null) 'name': name,
    });
  }

  LandTypesCompanion copyWith({Value<String> name}) {
    return LandTypesCompanion(
      name: name ?? this.name,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('LandTypesCompanion(')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }
}

class LandTypes extends Table with TableInfo<LandTypes, LandType> {
  final GeneratedDatabase _db;
  final String _alias;
  LandTypes(this._db, [this._alias]);
  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn('name', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  @override
  List<GeneratedColumn> get $columns => [name];
  @override
  LandTypes get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'land_types';
  @override
  final String actualTableName = 'land_types';
  @override
  VerificationContext validateIntegrity(Insertable<LandType> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name'], _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  LandType map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return LandType.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  LandTypes createAlias(String alias) {
    return LandTypes(_db, alias);
  }

  @override
  bool get dontWriteConstraints => true;
}

class LandClassification extends DataClass
    implements Insertable<LandClassification> {
  final String name;
  LandClassification({@required this.name});
  factory LandClassification.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    return LandClassification(
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String>(name);
    }
    return map;
  }

  LandClassificationsCompanion toCompanion(bool nullToAbsent) {
    return LandClassificationsCompanion(
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
    );
  }

  factory LandClassification.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return LandClassification(
      name: serializer.fromJson<String>(json['name']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'name': serializer.toJson<String>(name),
    };
  }

  LandClassification copyWith({String name}) => LandClassification(
        name: name ?? this.name,
      );
  @override
  String toString() {
    return (StringBuffer('LandClassification(')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf(name.hashCode);
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is LandClassification && other.name == this.name);
}

class LandClassificationsCompanion extends UpdateCompanion<LandClassification> {
  final Value<String> name;
  const LandClassificationsCompanion({
    this.name = const Value.absent(),
  });
  LandClassificationsCompanion.insert({
    @required String name,
  }) : name = Value(name);
  static Insertable<LandClassification> custom({
    Expression<String> name,
  }) {
    return RawValuesInsertable({
      if (name != null) 'name': name,
    });
  }

  LandClassificationsCompanion copyWith({Value<String> name}) {
    return LandClassificationsCompanion(
      name: name ?? this.name,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('LandClassificationsCompanion(')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }
}

class LandClassifications extends Table
    with TableInfo<LandClassifications, LandClassification> {
  final GeneratedDatabase _db;
  final String _alias;
  LandClassifications(this._db, [this._alias]);
  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn('name', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  @override
  List<GeneratedColumn> get $columns => [name];
  @override
  LandClassifications get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'land_classifications';
  @override
  final String actualTableName = 'land_classifications';
  @override
  VerificationContext validateIntegrity(Insertable<LandClassification> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name'], _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  LandClassification map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return LandClassification.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  LandClassifications createAlias(String alias) {
    return LandClassifications(_db, alias);
  }

  @override
  bool get dontWriteConstraints => true;
}

abstract class _$PerpDatabase extends GeneratedDatabase {
  _$PerpDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  Villages _villages;
  Villages get villages => _villages ??= Villages(this);
  Properties _properties;
  Properties get properties => _properties ??= Properties(this);
  Structures _structures;
  Structures get structures => _structures ??= Structures(this);
  StructurePermissionTypes _structurePermissionTypes;
  StructurePermissionTypes get structurePermissionTypes =>
      _structurePermissionTypes ??= StructurePermissionTypes(this);
  StructureClassifications _structureClassifications;
  StructureClassifications get structureClassifications =>
      _structureClassifications ??= StructureClassifications(this);
  StructureBuildTypes _structureBuildTypes;
  StructureBuildTypes get structureBuildTypes =>
      _structureBuildTypes ??= StructureBuildTypes(this);
  LandTypes _landTypes;
  LandTypes get landTypes => _landTypes ??= LandTypes(this);
  LandClassifications _landClassifications;
  LandClassifications get landClassifications =>
      _landClassifications ??= LandClassifications(this);
  Selectable<int> countEntries() {
    return customSelect('SELECT COUNT(*) as count FROM properties',
        variables: [],
        readsFrom: {properties}).map((QueryRow row) => row.readInt('count'));
  }

  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [
        villages,
        properties,
        structures,
        structurePermissionTypes,
        structureClassifications,
        structureBuildTypes,
        landTypes,
        landClassifications
      ];
  @override
  StreamQueryUpdateRules get streamUpdateRules => const StreamQueryUpdateRules(
        [
          WritePropagation(
            on: TableUpdateQuery.onTableName('villages',
                limitUpdateKind: UpdateKind.delete),
            result: [
              TableUpdate('properties', kind: UpdateKind.update),
            ],
          ),
          WritePropagation(
            on: TableUpdateQuery.onTableName('properties',
                limitUpdateKind: UpdateKind.delete),
            result: [
              TableUpdate('structures', kind: UpdateKind.delete),
            ],
          ),
        ],
      );
}
