package com.perp

import androidx.annotation.NonNull
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import android.content.Context
import android.os.Environment
import io.flutter.plugins.GeneratedPluginRegistrant
import io.flutter.plugins.GeneratedPluginRegistrant.*

class MainActivity : FlutterActivity() {
  private val CHANNEL = "com.ikraftsoft.perp"

  override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
    super.configureFlutterEngine(flutterEngine)
    MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler { call, result ->
      if (call.method == "getExternalFilesDir") {
        result.success(Environment.getExternalStorageDirectory().toString());
      } else {
        result.notImplemented()
      }
    }
    registerWith(flutterEngine)
  }

}
