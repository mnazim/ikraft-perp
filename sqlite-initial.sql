BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "properties" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	"owner"	TEXT NOT NULL,
	"parentage"	TEXT NOT NULL,
	"mobile_number"	TEXT NOT NULL,
	"location"	TEXT NOT NULL,
	"address"	TEXT NOT NULL,
	"district"	TEXT NOT NULL DEFAULT 'Anantnag',
	"village_code"	TEXT NOT NULL,
	"land_type"	TEXT NOT NULL,
	"land_classification"	TEXT NOT NULL,
	"distance_to_nala"	INTEGER NOT NULL,
	"khasra_number"	TEXT NOT NULL,
	"kanals"	INTEGER NOT NULL,
	"marlas"	INTEGER NOT NULL,
	"operator"	TEXT NOT NULL,
	"rev_official1"	TEXT NOT NULL,
	"rev_official2"	TEXT NOT NULL,
	"forrest_official"	TEXT NOT NULL,
	"created"	INTEGER NOT NULL,
	"modified"	INTEGER NOT NULL,
	"full_text"	TEXT NOT NULL,
	FOREIGN KEY("village_code") REFERENCES "villages"("code") ON DELETE SET NULL
);
CREATE TABLE IF NOT EXISTS "structures" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	"property_id"	INTEGER NOT NULL,
	"photos"	TEXT,
	"location"	TEXT,
	"build_type"	TEXT,
	"classification"	TEXT,
	"permission_type"	TEXT,
	"room_count"	INTEGER,
	"construction_year"	INTEGER,
	"area"	INTEGER,
	"operator"	TEXT,
	"distance_to_nala"	INTEGER,
	"rev_official1"	TEXT,
	"rev_official2"	TEXT,
	"forrest_official"	TEXT,
	"created"	INTEGER,
	"modified"	INTEGER,
	FOREIGN KEY("property_id") REFERENCES "properties"("id") ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS "villages" (
	"code"	TEXT NOT NULL,
	"name"	TEXT NOT NULL,
	"type"	TEXT NOT NULL,
	"tehsil"	TEXT NOT NULL,
	UNIQUE("name","type","tehsil"),
	PRIMARY KEY("code")
);
CREATE TABLE IF NOT EXISTS "structure_permission_types" (
	"name"	TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS "structure_classifications" (
	"name"	TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS "structure_build_types" (
	"name"	TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS "land_types" (
	"name"	TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS "land_classifications" (
	"name"	TEXT NOT NULL
);
INSERT INTO "properties" VALUES (1,'Govt. Middle School Edu. Dept.','','','','','Anantnag','2209','','',60,'Forest',0,10,'','','','',0,0,'*'),
 (2,'Gh Hassan','Rehman Sheikh','','','','Anantnag','2209','','',140,'363',0,4,'','','','',0,0,'*'),
 (3,'Fayaz Ahmad','Ali Mohd Shiekh','','','','Anantnag','2209','','',80,'144',0,5,'','','','',0,0,'*'),
 (4,'Abdul Salam Bhat','Gani Baht','','','','Anantnag','2209','','',140,'363',0,9,'','','','',0,0,'*'),
 (5,'Mohd Jabar','Ame Sheikh','','','','Anantnag','2209','','',145,'351',0,6,'','','','',0,0,'*'),
 (6,'Mohd Maqbool','Ame Sheikh','','','','Anantnag','2209','','',145,'351',0,8,'','','','',0,0,'*'),
 (7,'Mushtaq Ahmad','Gull Mohd Sheikh','','','','Anantnag','2209','','',148,'351',0,17,'','','','',0,0,'*'),
 (8,'Gh Nabi & Gh Hassan','Gull Mohd Sheikh','','','','Anantnag','2209','','',148,'351',0,14,'','','','',0,0,'*'),
 (9,'Mohd Shaif','Rehman Sheikh','','','','Anantnag','2209','','',85,'363',0,13,'','','','',0,0,'*'),
 (10,'Mohd Maqbool','Ame Sheikh','','','','Anantnag','2209','','',85,'363',0,13,'','','','',0,0,'*'),
 (11,'Ajaz Ahmad','Ame Sheikh','','','','Anantnag','2209','','',90,'363',0,8,'','','','',0,0,'*'),
 (12,'Farooq Ahmad','Ramzan Sheikh','','','','Anantnag','2209','','',80,'363',0,9,'','','','',0,0,'*'),
 (13,'Abdul Ahad','Ramzan Sheikh','','','','Anantnag','2209','','',80,'363',0,6,'','','','',0,0,'*'),
 (14,'Gh Rasool','Muma Lone','','','','Anantnag','2209','','',130,'166',0,16,'','','','',0,0,'*'),
 (15,'Muma Lone & Bilal Ahmad','Ali Lone & Muma Lone','','','','Anantnag','2209','','',145,'167',0,10,'','','','',0,0,'*'),
 (16,'Abdul Majid','Ame Sheikh','','','','Anantnag','2209','','',145,'167',0,2,'','','','',0,0,'*'),
 (17,'Ab Majid','Ame Sheikh','','','','Anantnag','2209','','',145,'167',0,19,'','','','',0,0,'*'),
 (18,'Abdul Rashid','Fateh Baba','','','','Anantnag','2209','','',145,'167',0,15,'','','','',0,0,'*'),
 (19,'Gulam Sheikh','Ramzan Shiekh','','','','Anantnag','2209','','',145,'167',0,14,'','','','',0,0,'*'),
 (20,'Abdul Waha','','','','','Anantnag','2209','','',140,'167',0,12,'','','','',0,0,'*'),
 (21,'Ismail Sheikh','','','','','Anantnag','2209','','',140,'167',0,3,'','','','',0,0,'*'),
 (22,'Anwar Sheikh','','','','','Anantnag','2209','','',140,'175',0,9,'','','','',0,0,'*'),
 (23,'Hassan Sheikh','Gani Shiekh','','','','Anantnag','2209','','',140,'168',0,9,'','','','',0,0,'*'),
 (24,'Mohd Ashraf','Gani Shiekh','','','','Anantnag','2209','','',140,'168',0,17,'','','','',0,0,'*'),
 (25,'Mohd Razan','Gh Nabi Sheikh','','','','Anantnag','2209','','',145,'168',0,12,'','','','',0,0,'*'),
 (26,'Abdul Rashid','Ame Sheikh','','','','Anantnag','2209','','',145,'175',0,9,'','','','',0,0,'*'),
 (27,'Nisar Sheikh','Salam Sheikh','','','','Anantnag','2209','','',145,'175',0,2,'','','','',0,0,'*'),
 (28,'Abdul Salam','Qadir Sheikh','','','','Anantnag','2209','','',140,'175',0,13,'','','','',0,0,'*'),
 (29,'Abdul Rashid','Salam Sheikh','','','','Anantnag','2209','','',140,'175',0,3,'','','','',0,0,'*'),
 (30,'Mohd Abdullah','Abdul Ahad Sheikh','','','','Anantnag','2209','','',140,'175',0,4,'','','','',0,0,'*'),
 (31,'Mst. Fata','Mst. Saja','','','','Anantnag','2209','','',135,'179',0,7,'','','','',0,0,'*'),
 (32,'Mohd Jabar','Ali Sheikh','','','','Anantnag','2209','','',145,'195',0,3,'','','','',0,0,'*'),
 (33,'Gh Hassan','Ramzan Sheikh','','','','Anantnag','2209','','',139,'195',0,13,'','','','',0,0,'*'),
 (34,'Imtiyaz Ahamd','Rajab Sheikh','','','','Anantnag','2209','','',139,'195',0,9,'','','','',0,0,'*'),
 (35,'Mohd Ashraf','Ramazan Sheikh','','','','Anantnag','2209','','',139,'195',0,7,'','','','',0,0,'*'),
 (36,'Mohd Yousuf','Ame Sheikh','','','','Anantnag','2209','','',139,'195',0,3,'','','','',0,0,'*'),
 (37,'Pahalgam Dev. Authority (Beetab Valley)','','','','','Anantnag','2209','','',145,'195',0,8,'','','','',0,0,'*'),
 (38,'Mst. Saleema','','','','','Anantnag','2209','','',145,'194',0,8,'','','','',0,0,'*');
INSERT INTO "structures" VALUES (1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'60',NULL,NULL,NULL,NULL,NULL,0),
 (2,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'140',NULL,NULL,NULL,NULL,NULL,0),
 (3,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'80',NULL,NULL,NULL,NULL,NULL,0),
 (4,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'140',NULL,NULL,NULL,NULL,NULL,0),
 (5,5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'145',NULL,NULL,NULL,NULL,NULL,0),
 (6,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'145',NULL,NULL,NULL,NULL,NULL,0),
 (7,7,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'148',NULL,NULL,NULL,NULL,NULL,0),
 (8,8,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'148',NULL,NULL,NULL,NULL,NULL,0),
 (9,9,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'85',NULL,NULL,NULL,NULL,NULL,0),
 (10,9,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'85',NULL,NULL,NULL,NULL,NULL,0),
 (11,10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'90',NULL,NULL,NULL,NULL,NULL,0),
 (12,11,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'80',NULL,NULL,NULL,NULL,NULL,0),
 (13,11,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'80',NULL,NULL,NULL,NULL,NULL,0),
 (14,12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'130',NULL,NULL,NULL,NULL,NULL,0),
 (15,13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'145',NULL,NULL,NULL,NULL,NULL,0),
 (16,13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'145',NULL,NULL,NULL,NULL,NULL,0),
 (17,14,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'145',NULL,NULL,NULL,NULL,NULL,0),
 (18,15,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'145',NULL,NULL,NULL,NULL,NULL,0),
 (19,15,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'145',NULL,NULL,NULL,NULL,NULL,0),
 (20,16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'140',NULL,NULL,NULL,NULL,NULL,0),
 (21,17,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'140',NULL,NULL,NULL,NULL,NULL,0),
 (22,18,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'140',NULL,NULL,NULL,NULL,NULL,0),
 (23,19,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'140',NULL,NULL,NULL,NULL,NULL,0),
 (24,20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'140',NULL,NULL,NULL,NULL,NULL,0),
 (25,21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'145',NULL,NULL,NULL,NULL,NULL,0),
 (26,22,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'145',NULL,NULL,NULL,NULL,NULL,0),
 (27,22,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'145',NULL,NULL,NULL,NULL,NULL,0),
 (28,23,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'140',NULL,NULL,NULL,NULL,NULL,0),
 (29,24,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'140',NULL,NULL,NULL,NULL,NULL,0),
 (30,24,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'140',NULL,NULL,NULL,NULL,NULL,0),
 (31,25,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'135',NULL,NULL,NULL,NULL,NULL,0),
 (32,26,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'145',NULL,NULL,NULL,NULL,NULL,0),
 (33,27,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'139',NULL,NULL,NULL,NULL,NULL,0),
 (34,28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'139',NULL,NULL,NULL,NULL,NULL,0),
 (35,29,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'139',NULL,NULL,NULL,NULL,NULL,0),
 (36,29,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'139',NULL,NULL,NULL,NULL,NULL,0),
 (37,30,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'145',NULL,NULL,NULL,NULL,NULL,0),
 (38,31,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'145',NULL,NULL,NULL,NULL,NULL,0),
 (39,32,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'149',NULL,NULL,NULL,NULL,NULL,0),
 (40,33,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'149',NULL,NULL,NULL,NULL,NULL,0),
 (41,34,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'149',NULL,NULL,NULL,NULL,NULL,0),
 (42,35,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'145',NULL,NULL,NULL,NULL,NULL,0),
 (43,36,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'135',NULL,NULL,NULL,NULL,NULL,0),
 (44,37,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'5',NULL,NULL,NULL,NULL,NULL,0),
 (45,38,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'100',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO "villages" VALUES ('3301','Aathanadan','Forest','Pahalgam'),
 ('3305','Bradihaji','Forest','Pahalgam'),
 ('3306','Brinward','Forest','Pahalgam'),
 ('3307','Budwan','Forest','Sallar'),
 ('3308','Chandanwari','Forest','Pahalgam'),
 ('3309','Frislana','Forest','Pahalgam'),
 ('3310','Ganishbal','Forest','Pahalgam'),
 ('3311','Gujran Batakote','Forest','Pahalgam'),
 ('3313','Hokhad','Forest','Pahalgam'),
 ('3316','Lalwani','Forest','Pahalgam'),
 ('3318','Larripora','Forest','Pahalgam'),
 ('3320','Market Pahalgam','Forest','Pahalgam'),
 ('3322','Mir Pora','Forest','Pahalgam'),
 ('3323','Mundlana','Forest','Pahalgam'),
 ('3326','Pissu Top','Forest','Pahalgam'),
 ('3327','Poshpathri','Forest','Pahalgam'),
 ('3328','Ragh Dar Lidroo','Forest','Pahalgam'),
 ('3329','Rangward','Forest','Pahalgam'),
 ('3330','Reanchpal','Forest','Pahalgam'),
 ('3332','Sheshnag','Forest','Pahalgam'),
 ('3335','Wang Dar Lidroo','Forest','Pahalgam'),
 ('3336','Wawbal & Zajipal','Forest','Pahalgam'),
 ('2202','Amad Wagad','General','Pahalgam'),
 ('2203','Aru','General','Pahalgam'),
 ('2204','Batakoot','General','Pahalgam'),
 ('2209','Frislana','General','Pahalgam'),
 ('2212','Hardu Kitchroo','General','Pahalgam'),
 ('2214','Khalan','General','Pahalgam'),
 ('2215','Khelan Gojran','General','Sallar'),
 ('2217','Laripora','General','Pahalgam'),
 ('2219','Lidroo','General','Pahalgam'),
 ('2220','Mamal','General','Pahalgam'),
 ('2221','Mavora','General','Sallar'),
 ('2223','Mundlana','General','Pahalgam'),
 ('2224','Nunwanan','General','Pahalgam'),
 ('2225','Pahalgam','General','Pahalgam'),
 ('2231','Sallar (Land Only)','General','Sallar'),
 ('2233','Srachin','General','Pahalgam'),
 ('2234','Veersaran','General','Sallar'),
 ('2237','Yenneed','General','Sallar');
INSERT INTO "structure_permission_types" VALUES ('No permission'),
 ('PDA'),
 ('MC'),
 ('Other');
INSERT INTO "structure_classifications" VALUES ('Residential'),
 ('Commercial'),
 ('Government'),
 ('Religious'),
 ('Other');
INSERT INTO "structure_build_types" VALUES ('Pucca'),
 ('Kacha'),
 ('Cow Shed'),
 ('Shed'),
 ('Other');
INSERT INTO "land_types" VALUES ('Agriculture'),
 ('Bagati'),
 ('Banjar'),
 ('Abaad'),
 ('Gair abad'),
 ('Other');
INSERT INTO "land_classifications" VALUES ('Government'),
 ('Milkiyat'),
 ('Forest'),
 ('Dafa 5'),
 ('Shamilat'),
 ('Gass charai'),
 ('Other');
COMMIT;
